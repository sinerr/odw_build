 #!perl

#------------------------------------------
# Get the System Modules
#------------------------------------------
use strict;
use DBI;
use Date::Calc qw(:all);
use Time::localtime;
use POSIX qw(ceil floor);
use Getopt::Long;
use Getopt::Long;
use Data::Dumper;

#------------------------------------------
# Next, load OST common routines
#------------------------------------------
use lib "../cgi-bin/ost_lib/";#OST common perl modules
use ost_cpconn;#library for CP functions
use ost_cpqbconn;

#------------------------------------------
# Last, load cpmgr appliation specific modules
#------------------------------------------
#use lib "../cgi-bin/cpmgr/";#CPMgr Library

use vars qw($QB_vendor_hash $QB_customer_hash $RPT_resource_hash $WE_hash $ods_dbflag $ods_schema $ods_tbl_suffix);
$ods_schema="";     # This will be set in the connection open function.
$ods_tbl_suffix=""; # This will be set in the connection open function.
$ods_dbflag="";     # This is selected in the command line

use vars qw($odsdb);
$odsdb ={'prod'  => {'dbflag' => 'prod'},
         'stage' => {'dbflag' => 'prod_stage'},
         'local' => {'dbflag' => 'test_local'},
         'test'  => {'dbflag' => 'test_server'},
        };

use vars qw($CPHolidays);
$CPHolidays=();

#use vars qw($CurDateTime $CurDate %debughash $tm $odsdbh $cpdbh);
use vars qw($CurDateTime $CurDate %debughash $tm $odsdbh);
%debughash=();

#--------------------------------------------------------------------------------
#Set the start and stop dates for the build
#--------------------------------------------------------------------------------
my($start,$end,$months,$db);
my $ival=&GetOptions ('start:s' => \$start,'end:s' =>\$end, 'months:s' => \$months, 'db:s' => \$db);
$months *=1;$months=3 unless($months > 0);

#Get target database and make sure a valid one is supplied
$db=lc($db)||"";
unless(defined($odsdb->{$db}{'dbflag'})){
    print "\nCommand missing one or more parameters....\n\n";
    print "Correct format is: $0 -<param> <value>\n";
    print "Valid parameters are:\n";
    print "\t  -start: Startdate\n";
    print "\t    -end: Ending Date\n";
    print "\t -months: Number of months\n";
    print "\t     -db: Database to be updated\n";
    print "-----------------------------------\n";
    print "\nInvalid or missing Database (db) flag......\n\n";
    print "Valid options are:\n\n";
    foreach my $key(sort{$a cmp $b} keys %{$odsdb}){
        next if $key eq "";
        print "\t- ".$key."\n";
    }
    print"\n\n";
    die;   
}
$ods_dbflag= $odsdb->{$db}{'dbflag'};

my($periodstart,$periodstop,$dim,$m,$d,$y,$calcmode);
if($start and $end){
    $calcmode="With start and end dates supplied....";
    ($m,$d,$y)=split(/\//,$start);
    $periodstart = sprintf('%02d/%02d/%04d',$m,1,$y);
    ($m,$d,$y)=split(/\//,$end);
    $dim=Days_in_Month($y,$m);
    $periodstop = sprintf('%02d/%02d/%04d',$m,$dim,$y);
}elsif($start){
    $calcmode="With only starting date supplied....";
    ($m,$d,$y)=split(/\//,$start);
    $periodstart = sprintf('%02d/%02d/%04d',$m,$d,$y);
    ($y,$m,$d)=Add_Delta_YM($y,$m,$d,0,$months-1);
    $dim=Days_in_Month($y,$m);
    $periodstop = sprintf('%02d/%02d/%04d',$m,$dim,$y);
}elsif($end){
    $calcmode="With only ending date supplied....";
    ($m,$d,$y)=split(/\//,$end);
    $dim=Days_in_Month($y,$m);
    $periodstop = sprintf('%02d/%02d/%04d',$m,$d,$y);
    ($y,$m,$d)=Add_Delta_YM($y,$m,$d,0,-1*($months-1));
    $periodstart = sprintf('%02d/%02d/%04d',$m,1,$y);
}else{
    $calcmode="With neither start or end dates supplied....";
    ($y,$m,$d)=Today();
    #set stop date to the end of the current month
    $dim=Days_in_Month($y,$m);
    $periodstop = sprintf('%02d/%02d/%04d',$m,$dim,$y);
    #Move start date back 2 months.
    ($y,$m,$d)=Add_Delta_YM($y,$m,$d,0,-1*($months-1));
    $periodstart = sprintf('%02d/%02d/%04d',$m,1,$y);
}

#print "\n".$ods_dbflag."\t".$calcmode."\t".$periodstart." to ".$periodstop."\n";die;

#--------------------------------------------------------------------------------
#Start the processing
#--------------------------------------------------------------------------------
&open_ODS_dbconnection() unless defined $odsdbh;
my($sql,$sth);
#$sql = "delete from ost_ods.dbo.build_info;insert into ost_ods.dbo.build_info (build_start) values (getdate())";
$sql = "delete from ".$ods_schema.".dbo.build_info".$ods_tbl_suffix."; insert into ".$ods_schema.".dbo.build_info".$ods_tbl_suffix." (build_start) values (getdate())";
$sth = $odsdbh->prepare($sql);
$sth->execute;
$sth->finish();

($y,$m,$d)=Today();#reset to current date
my $dow = Day_of_Week($y,$m,$d);
if($dow < 7){($y,$m,$d)=Add_Delta_Days($y,$m,$d,-1*$dow);}
my $curbusweek = sprintf('%02d/%02d/%04d',$m,$d,$y);

my $StartDateTime=&get_date_timestamp();
print "\n\n";
print "--------------------------------------------\n";
print "Refreshing ODS Services Information\n\n";
print "Target Database Flag set to $ods_dbflag\n";
print "Refresh Data range is from $periodstart to $periodstop \n\n";
print "--------------------------------------------\n";
print "Starting run ... ".&get_date_timestamp()."\n";
print "--------------------------------------------\n";
print "\n\n";

#------------------------------------------------------------------------
#&build_utilization_information($periodstart,$periodstop);die;
#print "Updating Primary Function Information....\n";
#&build_primary_function_information();
#print ".... Completed ".&get_date_timestamp()."\n\n";
#die;
#print "Updating Resource Information....\n";
#&build_resource_information();
#print ".... Completed ".&get_date_timestamp()."\n\n";
#die;
#print "Updating Workgroup Information....\n";
#&build_workgroup_information();
#print ".... Completed ".&get_date_timestamp()."\n\n";
#die;

#print "Updating Availability Information....".&get_date_timestamp()."\n";
#&build_availability_info($periodstart,$periodstop);
#print ".... Completed ".&get_date_timestamp()."\n\n";
#die;
#------------------------------------------------------------------------

#------------------------------------------------------------------------
print "Updating Customer Information....".&get_date_timestamp()."\n";
&build_customer_information();
print ".... Completed ".&get_date_timestamp()."\n\n";

#------------------------------------------------------------------------
print "Updating Workgroup Information....".&get_date_timestamp()."\n";
&build_workgroup_information();
print ".... Completed ".&get_date_timestamp()."\n\n";

#------------------------------------------------------------------------
print "Updating Primary Function Information....".&get_date_timestamp()."\n";
&build_primary_function_information();
print ".... Completed ".&get_date_timestamp()."\n\n";

#------------------------------------------------------------------------
print "Updating Resource Information....".&get_date_timestamp()."\n";
&build_resource_information();
print ".... Completed ".&get_date_timestamp()."\n\n";

#------------------------------------------------------------------------
print "Updating Project Information....".&get_date_timestamp()."\n";
&build_project_information();
print ".... Completed ".&get_date_timestamp()."\n\n";

#------------------------------------------------------------------------
print "Updating Work Date Information....".&get_date_timestamp()."\n";
&build_workdate_info($periodstart,$periodstop);
print ".... Completed ".&get_date_timestamp()."\n\n";

#------------------------------------------------------------------------
print "Updating Expense Information....".&get_date_timestamp()."\n";
&build_expense_information($periodstart,$periodstop);
print ".... Completed ".&get_date_timestamp()."\n\n";

#------------------------------------------------------------------------
print "Updating Time Information....".&get_date_timestamp()."\n";
&build_time_information($periodstart,$periodstop);
print ".... Completed ".&get_date_timestamp()."\n\n";

#------------------------------------------------------------------------
print "Updating Availability Information....".&get_date_timestamp()."\n";
&build_availability_info($periodstart,$periodstop);
print ".... Completed ".&get_date_timestamp()."\n\n";

#------------------------------------------------------------------------
print "Updating Utilization Information....\n";
print ".... Building base utilization records.....".&get_date_timestamp()."\n";
&build_utilization_information($periodstart,$periodstop);
print ".... Building period records.....".&get_date_timestamp()."\n";
&build_utilization_control($periodstart,$periodstop);
print ".... Utilization Information Update Completed ".&get_date_timestamp()."\n\n";

#------------------------------------------------------------------------
$sql = "update ".$ods_schema.".dbo.build_info".$ods_tbl_suffix." set build_stop = getdate()";
$sth = $odsdbh->prepare($sql);
$sth->execute;
$sth->finish();

my $EndDateTime=&get_date_timestamp();
print "\n\n";
print "--------------------------------------------\n";
print "Ending Run ...\n\n";
#print "Source CP Database Flag set to $cpdbflag\n";
print "Target ODS Database Flag set to $ods_dbflag\n";
print "Refresh Data range is from $periodstart to $periodstop\n";
print "Run started at ... ".$StartDateTime."\n";
print "Run ended at ... ".$EndDateTime."\n";
print "--------------------------------------------\n";
print "\n\n";

#------------------------------------------
sub get_date_timestamp {
    my $tm = localtime;
    return sprintf("%02d/%02d/%04d at %02d:%02d:%02d",$tm->mon + 1,$tm->mday,$tm->year+1900,$tm->hour,$tm->min,$tm->sec);
}#end get_date_timestamp

#------------------------------------------
sub build_utilization_control {
    my($sdate,$edate)=@_;
    my($m1,$d1,$y1)=split(/\//,$sdate);
    my($m2,$d2,$y2)=split(/\//,$edate);
    my($buildstart,$buildend,$days);
    my($bm1,$by1,$bd1,$bm2,$by2,$bd2,$ctrl1,$ctrl2,$qtrstart,$qtrend);

    #----------------------------------------------
    #Run the quarterly numbers
    $bm1=$m1;$by1=$y1;
    my($qyear,$qnbr,$qend,$qcurr);

    #First, set the ending quarter information
    $qyear=$y2+1;
       if($m2<4){$qnbr=4;$qyear=$y2;}
    elsif($m2<7){$qnbr=1;}
    elsif($m2<10){$qnbr=2;}
    else{$qnbr=3;}
    my $eqyear=$qyear; my $eqnbr=$qnbr;
    $qend=sprintf('%.4d%.2d',$eqyear,$eqnbr);

    #Next, set the starting quarter information
    $qyear=$y1+1;
       if($m1<4){$qnbr=4;$qyear=$y1;}
    elsif($m1<7){$qnbr=1;}
    elsif($m1<10){$qnbr=2;}
    else{$qnbr=3;}
    $qcurr=sprintf('%.4d%.2d',$qyear,$qnbr);

    my($qtr);
    my %qtrhash=(1=>{smonth=>4, offset=>-1},
                 2=>{smonth=>7, offset=>-1},
                 3=>{smonth=>10,offset=>-1},
                 4=>{smonth=>1, offset=> 0},
                );

        print ".. Quarterly Utilization Build for Q".$qcurr." --- Q".$qend." ".&get_date_timestamp()."\n";
    while($qcurr <= $qend){
        $qtr=sprintf('%s-Q%s',$qyear,$qnbr);
        $buildstart=sprintf('%s/%s/%s',$qtrhash{$qnbr}{smonth},1,$qyear + $qtrhash{$qnbr}{offset});
        $buildend=sprintf('%s/%s/%s',$qtrhash{$qnbr}{smonth}+2,Days_in_Month($qyear,$qtrhash{$qnbr}{smonth}+2),$qyear + $qtrhash{$qnbr}{offset});
        print ".... Building Quarterly Utilization for ".$qtr." (".join(' through ',$buildstart,$buildend)." - Resource\n";
        &build_utilization_numbers($buildstart,$buildend,'Q','R');
        print ".... Building Quarterly Utilization for ".$qtr." (".join(' through ',$buildstart,$buildend)." - Workgroup\n";
        &build_utilization_numbers($buildstart,$buildend,'Q','W');
        print ".... Building Quarterly Utilization for ".$qtr." (".join(' through ',$buildstart,$buildend)." - Primary Function\n";
        &build_utilization_numbers($buildstart,$buildend,'Q','P');
        #Go to next quarter
        if($qnbr < 4){$qnbr++;}
        else{$qyear++;$qnbr=1;}
        $qcurr=sprintf('%.4d%.2d',$qyear,$qnbr);
    }

    #----------------------------------------------
    #Run the monthly numbers
    $bm1=$m1;
    $by1=$y1;
    $ctrl1=sprintf('%.4d%.2d',$by1,$bm1);
    $ctrl2=sprintf('%.4d%.2d',$y2,$m2);
    print ".. Monthly Utilization build....".&get_date_timestamp()."\n";
    while ($ctrl1 <= $ctrl2){
        $days=Days_in_Month($by1,$bm1);
        $buildstart=sprintf('%.2d/%.2d/%.4d',$bm1,1,$by1);
        $buildend = sprintf('%.2d/%.2d/%.4d',$bm1,$days,$by1);
        print ".... Building Monthly Utilization for the month of ".$buildstart." - Resource\n";
        &build_utilization_numbers($buildstart,$buildend,'M','R');
        print ".... Building Monthly Utilization for the month of ".$buildstart." - Workgroup\n";
        &build_utilization_numbers($buildstart,$buildend,'M','W');
        print ".... Building Monthly Utilization for the month of ".$buildstart." - Primary Function\n";
        &build_utilization_numbers($buildstart,$buildend,'M','P');
        #Go to next month
        $bm1++;
        if($bm1 > 12){$bm1=1;$by1++;}
        $ctrl1=sprintf('%.4d%.2d',$by1,$bm1);
    }

    #----------------------------------------------
    #Run the weekly numbers
    ($by1,$bm1,$bd1)=Monday_of_Week(Week_of_Year($y1,$m1,$d1));
    $ctrl1=sprintf('%.4d%.2d%.2d',$by1,$bm1,$d1);
    $ctrl2=sprintf('%.4d%.2d%.2d',$y2,$m2,$d2);
    print ".. Weekly Utilization build....".&get_date_timestamp()."\n";
    while ($ctrl1 <= $ctrl2){
        ($by2,$bm2,$bd2)=Add_Delta_Days($by1,$bm1,$bd1,6);
        $buildstart=sprintf('%.2d/%.2d/%.4d',$bm1,$bd1,$by1);
        $buildend = sprintf('%.2d/%.2d/%.4d',$bm2,$bd2,$by2);
        print ".... Building Weekly Utilization for the week starting ".$buildstart." - Resource\n";
        &build_utilization_numbers($buildstart,$buildend,'W','R');
        print ".... Building Weekly Utilization for the week starting ".$buildstart." - Workgroup\n";
        &build_utilization_numbers($buildstart,$buildend,'W','W');
        print ".... Building Weekly Utilization for the week starting ".$buildstart." - Primary Function\n";
        &build_utilization_numbers($buildstart,$buildend,'W','P');
        #Go to next week
        ($by1,$bm1,$bd1)=Add_Delta_Days($by2,$bm2,$bd2,1);
        $ctrl1=sprintf('%.4d%.2d%.2d',$by1,$bm1,$bd1);
    }
    
}#build_utilization_control

#------------------------------------------
sub build_utilization_numbers {
    my($sdate,$edate,$buildtype,$scopetype)=@_;
    $buildtype=uc($buildtype);
    $scopetype=uc($scopetype);
    my %buildctrl=('W'=>{type=>'Weekly'},
                   'M'=>{type=>'Monthly'},
                   'Q'=>{type=>'Quarterly'},
                   );
    my %scopectrl=('W'=>{type=>'Workgroup',         tablename=>$ods_schema.".dbo.utiliz_workgroup".$ods_tbl_suffix},
                   'R'=>{type=>'Resource',          tablename=>$ods_schema.".dbo.utiliz_resource".$ods_tbl_suffix},
                   'P'=>{type=>'Primary Function',  tablename=>$ods_schema.".dbo.utiliz_primaryfunction".$ods_tbl_suffix},
                   );
    return unless (defined($scopectrl{$scopetype}{type})
              and  defined($buildctrl{$buildtype}{type}) 
              );

    my($sql,$sth);
    &open_ODS_dbconnection() unless defined $odsdbh;
    my $dbfmt_sdate = $odsdbh->quote($sdate);
    my $dbfmt_edate = $odsdbh->quote($edate);
    my $dbfmt_btype = $odsdbh->quote($buildtype);
    my $dbfmt_stype = $odsdbh->quote($scopetype);

    my($sql,$sth);

    $sql = "
select ui.resourceid
      ,ui.resourcename -- as 'Staff Name'
      ,ui.workgroupid
      ,ui.workgroupname -- as 'Workgroup'

      ,ui.primaryfunctionname
      ,ui.primaryfunctionid

      ,ui.adminhours -- as 'Admin'
      ,ui.billablehours -- as 'Billable'
      ,ui.stdholidayhours -- as 'Holiday'
      ,ui.altholidayhours -- as 'Holiday Alt (PTO)'
      ,ui.mgdshours -- as 'Managed Services'
      ,ui.nonbillhours -- as 'Non- Billable'
      ,ui.NonBillUtilizEligHours 
      ,ui.ptohours -- as 'PTO'
      ,ui.unpaidleavehours -- as 'Unpaid Leave'
      ,ui.presaleshours -- as 'Pre-Sales'
      ,ui.presalesexpecttobill -- as 'expecttobill'
      ,ui.apprvdtrnghours -- as 'Training (apprvd)'
      ,ui.selftrnghours -- as 'Training (self)'
      ,ui.travelhours  -- as 'Travel'
      ,ui.totalhours -- as 'Total Hours Reported'
      ,ui.availhours -- as 'Avail Hours'
      ,ui.pctgutilbill -- as '% Util Billable'
      ,ui.Pctgutilbillpre -- as '% Util Bill + Presales'
      ,ui.WorkGrpBusHours -- as 'Rsrc Eff Bus Hrs'
      ,ui.PctgEffectiveUtil -- as '% Effective Util'
from ".$ods_schema.".dbo.fn_utilization_info".$ods_tbl_suffix."_v2($dbfmt_sdate,$dbfmt_edate,$dbfmt_stype) ui
";

    $sth = $odsdbh->prepare($sql);
    $sth->execute;
    my($dbhash);
    my(@flds,@vals,@sqllist)=();
    push @sqllist,"delete from ".$scopectrl{$scopetype}{tablename}." ".
                  "where periodstartdate = ".$dbfmt_sdate." and utilizationperiod = ".$dbfmt_btype;
    while (my $info = $sth->fetchrow_hashref('NAME_lc')){
        $dbhash=();
        if($scopetype eq "W"){
            $dbhash->{workgrpid}=$info->{workgroupid};
            $dbhash->{workgroupname}=$odsdbh->quote($info->{workgroupname});
            $dbhash->{workgrpbushours}=$info->{workgrpbushours}*1;
        }elsif($scopetype eq "R"){
            $dbhash->{workgrpbushours}=$info->{workgrpbushours}*1;
        }elsif($scopetype eq "P"){
            $dbhash->{primaryfunctionid}=$info->{primaryfunctionid};
            $dbhash->{primaryfunctionname}=$odsdbh->quote($info->{primaryfunctionname});
            $dbhash->{primaryfunctbushours}=$info->{workgrpbushours}*1;
        }
        $dbhash->{resourceid}=$info->{resourceid};
        $dbhash->{utilizationperiod}=$dbfmt_btype;
        $dbhash->{periodstartdate}=$dbfmt_sdate;
        $dbhash->{resourcename}=$odsdbh->quote($info->{resourcename});
        $dbhash->{adminhours}=$info->{adminhours}*1;
        $dbhash->{billablehours}=$info->{billablehours}*1;
        $dbhash->{stdholidayhours}=$info->{stdholidayhours}*1;
        $dbhash->{altholidayhours}=$info->{altholidayhours}*1;
        $dbhash->{mgdshours}=$info->{mgdshours}*1;
        $dbhash->{nonbillhours}=$info->{nonbillhours}*1;
        $dbhash->{nonbillutilizelighours}=$info->{nonbillutilizelighours}*1;
        $dbhash->{ptohours}=$info->{ptohours}*1;
        $dbhash->{unpaidleavehours}=$info->{unpaidleavehours}*1;
        $dbhash->{presaleshours}=$info->{presaleshours}*1;
        $dbhash->{presalesexpecttobill}=$info->{presalesexpecttobill}*1;
        $dbhash->{apprvdtrnghours}=$info->{apprvdtrnghours}*1;
        $dbhash->{selftrnghours}=$info->{selftrnghours}*1;
        $dbhash->{travelhours}=$info->{travelhours}*1;
        $dbhash->{availhours}=$info->{availhours}*1;
#        $dbhash->{workgrpbushours}=$info->{workgrpbushours}*1;
        $dbhash->{pctgutilbill}=$info->{pctgutilbill}*1;
        $dbhash->{pctgutilbillpresales}=$info->{pctgutilbillpre}*1;
        $dbhash->{pctgeffectiveutil}=$info->{pctgeffectiveutil}*1;
        @flds=@vals=();
        foreach my $key(keys %{$dbhash}){
            push @flds,$key;
            push @vals,$dbhash->{$key};
        }
        push @sqllist,"insert into ".$scopectrl{$scopetype}{tablename}." (".join(',',@flds).") values (".join(',',@vals).")";
    }
    $sth->finish();
    if(@sqllist){
        $sql = "begin transaction;set xact_abort on;".join(';',@sqllist).";commit transaction;";
        $sth = $odsdbh->prepare($sql);
        if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to prepare utilization information update query","Query - $sql");}
        $sth->execute;
        if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to execute utilization information update query","Query - $sql");}
        $sth->finish();
    }
    
}#end build_utilization_numbers

#------------------------------------------
sub get_whse_resource_information {
    #This retrieves the customer information from the ODS.
    #the return is a hash of hashes, with two sub-hashes allowing search by either CP custid or whse custid
    #For now, this will rebuild the hash each time it is called.  Later we'll want to add the rest of the logic if/when needed
#    my($force_rebuild)=@_;
#    $force_rebuild=uc($force_rebuild)||"Y";
#    $force_rebuild="Y" unless $force_rebuild eq "N";
    &open_ODS_dbconnection() unless defined $odsdbh;
    my($sql,$sth);
    $sql = "select resourceid, cpresourceid, resourcename, currentworkgroupid, currentprimaryfunctionid from ".$ods_schema.".dbo.resourceinfo".$ods_tbl_suffix;
    $sth = $odsdbh->prepare($sql);
    $sth->execute;
    my $rsrchash=();
    while (my $info = $sth->fetchrow_hashref('NAME_lc')){
        $rsrchash->{rsrcid}{$info->{resourceid}}{name}=$info->{resourcename};
        $rsrchash->{rsrcid}{$info->{resourceid}}{cpresourceid}=$info->{cpresourceid};
        $rsrchash->{rsrcid}{$info->{resourceid}}{currentwgid}=$info->{currentworkgroupid};
        $rsrchash->{rsrcid}{$info->{resourceid}}{currentfid}=$info->{currentprimaryfunctionid};

        $rsrchash->{cprsrcid}{$info->{cpresourceid}}{name}=$info->{resourcename};
        $rsrchash->{cprsrcid}{$info->{cpresourceid}}{rsrcid}=$info->{resourceid};
        $rsrchash->{cprsrcid}{$info->{cpresourceid}}{currentwgid}=$info->{currentworkgroupid};
        $rsrchash->{cprsrcid}{$info->{cpresourceid}}{currentfid}=$info->{currentprimaryfunctionid};
    }
    $sth->finish();

    #next, we'll get information known about the resource
    my($sdate,$edate)=@_;
    if($sdate ne "" and $edate ne ""){
        my $dbfmt_sdate = $odsdbh->quote($sdate);
        my $dbfmt_edate = $odsdbh->quote($edate);
        $sql = "
select t.resourceid
       ,t.workdate 
       ,convert(varchar(10),t.workdate,101) as workdate
       ,sum(case when t.timetypeid = 1 then t.hours else 0 end) as billable_hours
       ,sum(case when t.timetypeid = 2 then t.hours else 0 end) as presales_hours
       ,sum(case when t.timetypeid = 5 then t.hours else 0 end) as nonbillable_hours
       ,sum(case when t.timetypeid = 6 then t.hours else 0 end) as trngapproved_hours
       ,sum(case when t.timetypeid = 7 then t.hours else 0 end) as admin_hours
       ,sum(case when t.timetypeid = 8 then t.hours else 0 end) as travel_hours
       ,sum(case when t.timetypeid = 9 then t.hours else 0 end) as mgds_hours
       ,sum(case when t.timetypeid = 10 then t.hours else 0 end) as pto_hours
       ,sum(case when t.timetypeid = 11 then t.hours else 0 end) as nonbillutilizelig_hours
       ,sum(case when t.timetypeid = 16 then t.hours else 0 end) as trngself_hours
       ,sum(case when t.timetypeid = 20 then t.hours else 0 end) as altholiday_hours
       ,sum(case when t.timetypeid = 21 then t.hours else 0 end) as unpaidleave_hours
from ".$ods_schema.".dbo.timeinfo".$ods_tbl_suffix." t
where t.workdate between $dbfmt_sdate and $dbfmt_edate
group by t.resourceid, t.workdate
";
        $sth = $odsdbh->prepare($sql);
        $sth->execute;
        while (my $info = $sth->fetchrow_hashref('NAME_lc')){

            $rsrchash->{timeinfo}{$info->{resourceid}}{$info->{workdate}}{billable}=$info->{billable_hours};
            $rsrchash->{timeinfo}{$info->{resourceid}}{$info->{workdate}}{presales}=$info->{presales_hours};
            $rsrchash->{timeinfo}{$info->{resourceid}}{$info->{workdate}}{nonbill}=$info->{nonbillable_hours};
            $rsrchash->{timeinfo}{$info->{resourceid}}{$info->{workdate}}{nonbillutilizelig}=$info->{nonbillutilizelig_hours};
            $rsrchash->{timeinfo}{$info->{resourceid}}{$info->{workdate}}{trngapproved}=$info->{trngapproved_hours};
            $rsrchash->{timeinfo}{$info->{resourceid}}{$info->{workdate}}{admin}=$info->{admin_hours};
            $rsrchash->{timeinfo}{$info->{resourceid}}{$info->{workdate}}{travel}=$info->{travel_hours};
            $rsrchash->{timeinfo}{$info->{resourceid}}{$info->{workdate}}{mgds}=$info->{mgds_hours};
            $rsrchash->{timeinfo}{$info->{resourceid}}{$info->{workdate}}{pto}=$info->{pto_hours};
            $rsrchash->{timeinfo}{$info->{resourceid}}{$info->{workdate}}{trngself}=$info->{trngself_hours};
            $rsrchash->{timeinfo}{$info->{resourceid}}{$info->{workdate}}{altholiday}=$info->{altholiday_hours};
            $rsrchash->{timeinfo}{$info->{resourceid}}{$info->{workdate}}{unpaidleave}=$info->{unpaidleave_hours};
        }
        $sth->finish();
    }

    return($rsrchash);
}#end get_whse_resource_information

#------------------------------------------
sub build_resource_information {
    &open_cp_dbconnection unless defined $cpdbh;
    &open_ODS_dbconnection() unless defined $odsdbh;

    #Build basic resource information
    my $rsrchash=&get_whse_resource_information();
    my $wghash=&get_whse_workgroup_information();
    my $pfhash=&get_whse_primary_function_information();
    my $sql = "
select r.resourceid, r.name as resourcename, t.workdate as last_time_log_date,
       wg.name as currentwgname, wg.WorkgroupId as currentwgid, 
       r.primaryfunctionid
  from resources r
       left join workgroupmember wm on r.resourceid = wm.resourceid and wm.historical=0
       left join workgroup wg on wm.workgroupid = wg.workgroupid
       left join (select min(a.resourceid) as resourceid, max(a.workdate) as workdate
                    from (select resourceid, max(cast(timedate as date)) as workdate from time group by resourceid
                          union
                          select resourceid, max(cast(starttime as date)) as workdate from StandardTaskTime group by resourceid
                         )a 
                   group by a.resourceid
                 ) t on r.resourceid = t.resourceid
";
    my @cmds=();
    my (@cols,@val1,@val2)=();
    my ($modeflag,%colmap);
    my $sth = $cpdbh->prepare($sql);
    $sth->execute;
    while (my $info = $sth->fetchrow_hashref('NAME_lc')){
        %colmap=();
        $colmap{resourcename}=$odsdbh->quote($info->{resourcename});
        $colmap{cpresourceid}=$odsdbh->quote($info->{resourceid});
        $colmap{lastupdatedon}="getdate()";
        $colmap{currentworkgroupid}=$wghash->{cpwgid}{$info->{currentwgid}}{wgid}||0;$colmap{currentworkgroupid}*=1;
        $colmap{currentprimaryfunctionid}=$pfhash->{cpfid}{$info->{primaryfunctionid}}{fid}||0;$colmap{currentprimaryfunctionid}*=1;

        $modeflag="";#default to do nothing
        if(!defined($rsrchash->{cprsrcid}{$info->{resourceid}}{rsrcid})){$modeflag="A";}
        elsif($rsrchash->{cprsrcid}{$info->{resourceid}}{name} ne $info->{resourcename}){$modeflag = "U1";}
        elsif($rsrchash->{cprsrcid}{$info->{resourceid}}{currentwgid} != $colmap{currentworkgroupid}){$modeflag = "U2";}
        elsif($rsrchash->{cprsrcid}{$info->{resourceid}}{currentfid} != $colmap{currentprimaryfunctionid}){$modeflag = "U3";}

        if($modeflag eq ""){
            next;
        }elsif($modeflag eq "A"){#Add a new record
            @cols=@val1=@val2=();
            foreach my $key(keys %colmap){push @cols,$key;push @val1,$colmap{$key};}
            push @cmds, "insert into ".$ods_schema.".dbo.resourceinfo".$ods_tbl_suffix." (".join(',',@cols).") values (".join(',',@val1).")";
        }else{#Update the record
            @cols=();
            foreach my $key(keys %colmap){push @cols,$key." = ".$colmap{$key};}
            push @cmds,"update ".$ods_schema.".dbo.resourceinfo".$ods_tbl_suffix." set ".join(',',@cols).
                       " where resourceid = ".$rsrchash->{cprsrcid}{$info->{resourceid}}{rsrcid};
        }
    }
    $sth->finish();
    if(@cmds){#we have new resource records to add to the warehouse
#print Dumper($rsrchash->{cprsrcid});
print join("\n",@cmds)."\n\n";
        $sql = join(';',@cmds);
        $sth = $odsdbh->prepare($sql);
        $sth->execute;
        $sth->finish();
    }
}#end build_resource_information

#------------------------------------------
sub get_whse_customer_information {
    #This retrieves the customer information from the ODS.
    #the return is a hash of hashes, with two sub-hashes allowing search by either CP custid or whse custid
    #For now, this will rebuild the hash each time it is called.  Later we'll want to add the rest of the logic if/when needed
#    my($force_rebuild)=@_;
#    $force_rebuild=uc($force_rebuild)||"Y";
#    $force_rebuild="Y" unless $force_rebuild eq "N";
    &open_ODS_dbconnection() unless defined $odsdbh;
    my $sql = "select customerid, cpcustomerid, customername, mgdsflag, cornerstoneflag from ".$ods_schema.".dbo.customerinfo".$ods_tbl_suffix;
    my $sth = $odsdbh->prepare($sql);
    $sth->execute;
    my $chash=();
    while (my $info = $sth->fetchrow_hashref('NAME_lc')){
        $chash->{custid}{$info->{customerid}}{name}=$info->{customername};
        $chash->{custid}{$info->{customerid}}{cpcustomerid}=$info->{cpcustomerid};
        $chash->{custid}{$info->{customerid}}{mgdsflag}=$info->{mgdsflag};
        $chash->{custid}{$info->{customerid}}{cornerstoneflag}=$info->{cornerstoneflag};

        $chash->{cpcustid}{$info->{cpcustomerid}}{name}=$info->{customername};
        $chash->{cpcustid}{$info->{cpcustomerid}}{custid}=$info->{customerid};
        $chash->{cpcustid}{$info->{cpcustomerid}}{mgdsflag}=$info->{mgdsflag};
        $chash->{cpcustid}{$info->{cpcustomerid}}{cornerstoneflag}=$info->{cornerstoneflag};
        if($info->{customername} eq "OST"){
            $chash->{ostid}{custid}=$info->{customerid};
            $chash->{ostid}{cpcustid}=$info->{cpcustomerid};
        }
    }
    $sth->finish();
    return($chash);
}#end get_whse_customer_information

#------------------------------------------
sub build_customer_information {
    &open_cp_dbconnection unless defined $cpdbh;
    &open_ODS_dbconnection() unless defined $odsdbh;
    my $chash=&get_whse_customer_information();
    my $sql = "
select c.customerid
      ,c.name
      ,upper(isnull(mgds.UDFText,'N')) as MgdsFlag
      ,case cd.Description when 'Yes' then 'Cornerstone' when 'Potential' then 'Potential' else 'Non-Cornerstone' end as CornerstoneFlag
from customer c
     left join UDFCode uc on c.customerid = uc.EntityId and uc.ItemName = 'CustomerCode2'
     left join CodeDetail cd on uc.UDFCode = cd.CodeDetail
     left join udftext mgds on c.customerid = mgds.EntityId and mgds.itemname = 'CustomerText2'
";
    my $sth = $cpdbh->prepare($sql);
    $sth->execute;
    my @cmds=();
    my @updcmds=();
    while (my $info = $sth->fetchrow_hashref('NAME_lc')){
        if(not defined($chash->{cpcustid}{$info->{customerid}}{name})){
            push @cmds, "insert into ".$ods_schema.".dbo.customerinfo".$ods_tbl_suffix." (customername, cpcustomerid, mgdsflag, cornerstoneflag) values (".
                         $odsdbh->quote($info->{name}).",".
                         $odsdbh->quote($info->{customerid}).",".
                         $odsdbh->quote($info->{mgdsflag}).",".
                         $odsdbh->quote($info->{cornerstoneflag}).
                         ")";
        }else{#Customer exists, so we'll check for any necesary updates
            if($chash->{cpcustid}{$info->{customerid}}{cornerstoneflag} ne $info->{cornerstoneflag}){
                push @updcmds,"update ".$ods_schema.".dbo.customerinfo".$ods_tbl_suffix." set cornerstoneflag = ".$cpdbh->quote($info->{cornerstoneflag})." where customerid = ".$chash->{cpcustid}{$info->{customerid}}{custid};
            }
            if($chash->{cpcustid}{$info->{customerid}}{mgdsflag} ne $info->{mgdsflag}){
                push @updcmds,"update ".$ods_schema.".dbo.customerinfo".$ods_tbl_suffix." set mgdsflag = ".$cpdbh->quote($info->{mgdsflag})." where customerid = ".$chash->{cpcustid}{$info->{customerid}}{custid};
            }
        }
    }
    $sth->finish();
    $sql="";
    if(@cmds){#we have new customer records to add to the warehouse
        $sql .= join(';',@cmds);
        $sql .=";";
    }
    if(@updcmds){#We have updates to make
        $sql .= join(';',@updcmds);
        $sql .=";";
    }
    if($sql ne ""){
        $sth = $odsdbh->prepare($sql);
        $sth->execute;
        $sth->finish();
    }
}#end build_customer_information

#------------------------------------------
sub get_whse_workgroup_information {
    &open_ODS_dbconnection() unless defined $odsdbh;
    my $sql = "select workgroupid, cpworkgroupid, workgroupname from ".$ods_schema.".dbo.workgroupinfo".$ods_tbl_suffix;
    my $sth = $odsdbh->prepare($sql);
    $sth->execute;
    my $wghash=();
    while (my $info = $sth->fetchrow_hashref('NAME_lc')){
        $wghash->{wgid}{$info->{workgroupid}}{name}=$info->{workgroupname};
        $wghash->{wgid}{$info->{workgroupid}}{cpwgid}=$info->{cpworkgroupid};
        $wghash->{cpwgid}{$info->{cpworkgroupid}}{name}=$info->{workgroupname};
        $wghash->{cpwgid}{$info->{cpworkgroupid}}{wgid}=$info->{workgroupid};
    }
    $sth->finish();
    return($wghash);
}#end get_whse_workgroup_information

#------------------------------------------
sub build_workgroup_information {
    &open_cp_dbconnection unless defined $cpdbh;
    &open_ODS_dbconnection() unless defined $odsdbh;
    my $wghash=&get_whse_workgroup_information();
    my $sql = "select workgroupid, name, parent as parentworkgroupid from workgroup where deleted = 0";
    my $sth = $cpdbh->prepare($sql);
    $sth->execute;
    my @cmds=();
    my @updatecmds=();
    while (my $info = $sth->fetchrow_hashref('NAME_lc')){
        if(defined $wghash->{cpwgid}{$info->{workgroupid}}{name}){
            push @updatecmds,"update ".$ods_schema.".dbo.workgroupinfo".$ods_tbl_suffix." ".
                                "set workgroupname = ".$odsdbh->quote($info->{name}).",".
                                    "cpworkgroupid = ".$odsdbh->quote($info->{workgroupid}).",".
                                    "parentworkgroupid = ".$odsdbh->quote($info->{parentworkgroupid})." ".
                              "where workgroupid = ".$wghash->{cpwgid}{$info->{workgroupid}}{wgid};
        }else{
            push @cmds, "insert into ".$ods_schema.".dbo.workgroupinfo".$ods_tbl_suffix." (workgroupname, cpworkgroupid, parentworkgroupid) values (".$odsdbh->quote($info->{name}).",".$odsdbh->quote($info->{workgroupid}).",".$odsdbh->quote($info->{parentworkgroupid}).")";
        }
    }
    $sth->finish();
    $sql="";
    #we have new customer records to add to the warehouse
    if(@cmds){$sql .= join(';',@cmds)."; ";}
    if(@updatecmds){$sql .= join(';',@updatecmds)."; ";}
    if($sql){
        $sql = "begin transaction; set xact_abort on; ".$sql." commit transaction; ";
        $sth = $odsdbh->prepare($sql);
        if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to prepare Workgroup Info update query","Query - $sql");}
        $sth->execute;
        if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to execute Workgroup Info update query","Query - $sql");}
        $sth->finish();
    }
}#end build_workgroup_information

#------------------------------------------
sub get_whse_primary_function_information {
    &open_ODS_dbconnection() unless defined $odsdbh;
    my $sql = "select functionid, cpfunctionid, functionname, activeflag from ".$ods_schema.".dbo.primaryfunctioninfo".$ods_tbl_suffix;
    my $sth = $odsdbh->prepare($sql);
    $sth->execute;
    my $pfhash=();
    while (my $info = $sth->fetchrow_hashref('NAME_lc')){
        $pfhash->{fid}{$info->{functionid}}{name}       = $info->{functionname};
        $pfhash->{fid}{$info->{functionid}}{fid}        = $info->{functionid};
        $pfhash->{fid}{$info->{functionid}}{cpfid}      = $info->{cpfunctionid};
        $pfhash->{fid}{$info->{functionid}}{activeflag} = $info->{activeflag};

        $pfhash->{cpfid}{$info->{cpfunctionid}}{name}       = $info->{functionname};
        $pfhash->{cpfid}{$info->{cpfunctionid}}{fid}        = $info->{functionid};
        $pfhash->{cpfid}{$info->{cpfunctionid}}{cpfid}      = $info->{cpfunctionid};
        $pfhash->{cpfid}{$info->{cpfunctionid}}{activeflag} = $info->{activeflag};
    }
    $sth->finish();
    return($pfhash);
}#end get_whse_primary_function_information

#------------------------------------------
sub build_primary_function_information {
    &open_cp_dbconnection unless defined $cpdbh;
    &open_ODS_dbconnection() unless defined $odsdbh;
    my $pfhash=&get_whse_primary_function_information();
    my $sql = "select functionid, name, case when deleted = 0 then 1 else 0 end as active_flag from functiondescription order by name asc";
    my $sth = $cpdbh->prepare($sql);
    $sth->execute;
    my @cmds=();
    my @updatecmds=();
    while (my $info = $sth->fetchrow_hashref('NAME_lc')){
        if(defined $pfhash->{cpfid}{$info->{functionid}}{name}){
            push @updatecmds,"update ".$ods_schema.".dbo.primaryfunctioninfo".$ods_tbl_suffix." ".
                                "set cpfunctionid = ".$odsdbh->quote($info->{functionid}).",".
                                    "functionname = ".$odsdbh->quote($info->{name}).",".
                                      "activeflag = ".$info->{active_flag}.
                                "where functionid = ".$pfhash->{cpfid}{$info->{functionid}}{fid};
        }else{
            push @cmds, "insert into ".$ods_schema.".dbo.primaryfunctioninfo".$ods_tbl_suffix." ".
                        "(functionname, cpfunctionid, activeflag) values (".
                        $odsdbh->quote($info->{name}).",".
                        $odsdbh->quote($info->{functionid}).",".
                        $info->{active_flag}.
                        ")";
        }
    }
    $sth->finish();
    $sql="";
    if(@cmds){$sql .= join(';',@cmds)."; ";}
    if(@updatecmds){$sql .= join(';',@updatecmds)."; ";}
    if($sql){
        $sql = "begin transaction; set xact_abort on; ".$sql." commit transaction; ";
        $sth = $odsdbh->prepare($sql);
        if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to prepare Primary Function Info update query","Query - $sql");}
        $sth->execute;
        if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to execute Primary Function Info update query","Query - $sql");}
        $sth->finish();
        $sql =~ s/\;/\;\n/g;
#        print $sql."\n";
    }
}#end build_primary_function_information

#------------------------------------------
sub get_whse_project_information {
    &open_ODS_dbconnection() unless defined $odsdbh;
    my $sql = "select p.* from ".$ods_schema.".dbo.projectinfo".$ods_tbl_suffix." p";
    my $sth = $odsdbh->prepare($sql);
    $sth->execute;
    my $phash=();
    while (my $info = $sth->fetchrow_hashref('NAME_lc')){
        $phash->{cpprojid}{$info->{cpprojectid}}{name}=$info->{projectname};
        $phash->{cpprojid}{$info->{cpprojectid}}{projid}=$info->{projectid};

        $phash->{projid}{$info->{projectid}}{name}=$info->{projectname};
        $phash->{projid}{$info->{projectid}}{cpprojid}=$info->{cpprojectid};
        foreach my $key (keys %{$info}){
            $phash->{projid}{$info->{projectid}}{$key}=$info->{$key};
        }
    }
    $sth->finish();
    return($phash);
}#end get_whse_project_information

#------------------------------------------
sub build_project_information {
    &open_cp_dbconnection unless defined $cpdbh;
    &open_ODS_dbconnection() unless defined $odsdbh;
    my $phash=&get_whse_project_information();
    my $rhash=&get_whse_resource_information();
    my $sql = "
select p.name as project
      ,c.name as customer
      ,e.name as engagement
      ,case p.Billable when 1 then 'Y' else 'N' end as billable
      ,cdetpt.Description as practice
      ,r1.name as primary_account_exec,r1.resourceid as primary_account_exec_id
      ,r2.name as secondary_account_exec,r2.resourceid as secondary_account_exec_id
      ,r3.name as primary_tech_influencer,r3.resourceid as primary_tech_influencer_id
      ,r4.name as secondary_tech_influencer,r4.resourceid as secondary_tech_influencer_id
      ,r5.name as managing_consultant,r5.resourceid as managing_consultant_id
      ,r6.name as pmo_lead,r6.resourceid as pmo_lead_id
      ,utext1.udftext as SOW
      ,cast(isnull(p.labourbudget,0) as decimal(18,2)) as project_value
      ,cast(isnull(utext2.udfnumber,0) as decimal(18,2)) as SowHours
      ,ps.description as project_status
      ,cdet.description as project_reporting_type
      ,p.ProjectId
      ,p.CustomerId
      ,p.EngagementId
  from Project p
       left join ProjectStatus ps on p.ProjectStatus = ps.Code
       left join Customer c on p.customerid = c.customerid
       left join engagement e on p.engagementid = e.engagementid
       left join dbo.codedetail cdetpt on p.CPProjectType = cdetpt.CodeDetail and cdetpt.CodeType = 'CPProjectType'
       left join dbo.udfcode ucode on p.projectid = ucode.entityid and ucode.itemname = 'ProjectCode506' -- Project Reporting Type
       left join dbo.codedetail cdet on ucode.udfcode = cdet.codedetail
       left join dbo.udftext utext1 on p.projectid = utext1.entityid and utext1.itemname = 'ProjectText500' --- SOW ID
       left join dbo.udfnumber utext2 on p.projectid = utext2.entityid and utext2.itemname = 'ProjectText502' --- SOW Planned Hours
       left join dbo.udfcode ucode1 on p.projectid = ucode1.EntityId and ucode1.itemname = 'ProjectCode500' -- Primary Account Exec
       left join dbo.resources r1 on ucode1.udfcode = r1.resourceid
       left join dbo.udfcode ucode2 on p.projectid = ucode2.EntityId and ucode2.itemname = 'ProjectCode501' -- Secondary Account Exec
       left join dbo.resources r2 on ucode2.udfcode = r2.resourceid
       left join dbo.udfcode ucode3 on p.projectid = ucode3.EntityId and ucode3.itemname = 'ProjectCode502' -- Primary Tech Influencer
       left join dbo.resources r3 on ucode3.udfcode = r3.resourceid
       left join dbo.udfcode ucode4 on p.projectid = ucode4.EntityId and ucode4.itemname = 'ProjectCode503' -- Primary Tech Influencer
       left join dbo.resources r4 on ucode4.udfcode = r4.resourceid
       left join dbo.udfcode ucode5 on p.projectid = ucode5.EntityId and ucode5.itemname = 'ProjectCode507' -- Managing Consultant
       left join dbo.resources r5 on ucode5.udfcode = r5.resourceid
       left join dbo.udfcode ucode6 on p.projectid = ucode6.EntityId and ucode6.itemname = 'ProjectCode508' -- PMO Lead
       left join dbo.resources r6 on ucode6.udfcode = r6.resourceid
";
    my $sth = $cpdbh->prepare($sql);
    $sth->execute;
    my @cmds=();
    my @updcmds=();
    my(@cols,@val1,@val2);
    my (%colmap);
    while (my $info = $sth->fetchrow_hashref('NAME_lc')){
        %colmap=();
        
        #Base information
        $colmap{projectname}=$odsdbh->quote($info->{project});
        $colmap{customername}=$odsdbh->quote($info->{customer});
        $colmap{engagementname}=$odsdbh->quote($info->{engagement});

        #Base attributes
        $colmap{billable}=$odsdbh->quote($info->{billable});
        $colmap{projectstatus}=$odsdbh->quote($info->{project_status});
        $colmap{projectreportingtype}=$odsdbh->quote($info->{project_reporting_type});
        $colmap{practice}=$odsdbh->quote($info->{practice});
        $colmap{sow}=$odsdbh->quote($info->{sow});
        $colmap{sowhours}=$info->{sowhours}*1;
        $colmap{projectvalue}=$info->{project_value}*1;

        #Resource related information
        $colmap{primaryaccountexec}=$odsdbh->quote($info->{primary_account_exec});
        $colmap{secondaryaccountexec}=$odsdbh->quote($info->{secondary_account_exec});
        $colmap{primarytechinfluencer}=$odsdbh->quote($info->{primary_tech_influencer});
        $colmap{secondarytechinfluencer}=$odsdbh->quote($info->{secondary_tech_influencer});
        $colmap{managingconsultant}=$odsdbh->quote($info->{managing_consultant});
        $colmap{pmolead}=$odsdbh->quote($info->{pmo_lead});

        $colmap{primaryaccountexecid}=$odsdbh->quote($rhash->{cprsrcid}{$info->{primary_account_exec_id}}{rsrcid});
        $colmap{secondaryaccountexecid}=$odsdbh->quote($rhash->{cprsrcid}{$info->{secondary_account_exec_id}}{rsrcid});
        $colmap{primarytechinfluencerid}=$odsdbh->quote($rhash->{cprsrcid}{$info->{primary_tech_influencer_id}}{rsrcid});
        $colmap{secondarytechinfluencerid}=$odsdbh->quote($rhash->{cprsrcid}{$info->{secondary_tech_influencer_id}}{rsrcid});
        $colmap{managingconsultantid}=$odsdbh->quote($rhash->{cprsrcid}{$info->{managing_consultant_id}}{rsrcid});
        $colmap{pmoleadid}=$odsdbh->quote($rhash->{cprsrcid}{$info->{pmo_lead_id}}{rsrcid});

        #CP uniqueid items
        $colmap{cpprojectid}=$odsdbh->quote($info->{projectid});
        $colmap{cpengagementid}=$odsdbh->quote($info->{engagementid});
        $colmap{cpcustomerid}=$odsdbh->quote($info->{customerid});
        

        if(not defined($phash->{cpprojid}{$info->{projectid}}{name})){#This is a new project, so we need to add it to the table
            @cols=@val1=@val2=();
            foreach my $key(keys %colmap){
                push @cols,$key;
                push @val1,$colmap{$key};
            }
            push @cols,'lastupdatedon';push @val1,'getdate()';
            push @cmds, "insert into ".$ods_schema.".dbo.projectinfo".$ods_tbl_suffix." (".join(',',@cols).") values (".join(',',@val1).")";
        }else{#The project exists, so we'll do an update
            @cols=();
            foreach my $key(keys %colmap){
                push @cols,$key." = ".$colmap{$key};
            }
            push @cols,"lastupdatedon = getdate()";
            push @updcmds,"update ".$ods_schema.".dbo.projectinfo".$ods_tbl_suffix." set ".join(',',@cols)." where projectid = ".$phash->{cpprojid}{$info->{projectid}}{projid};
        }
    }
    $sth->finish();
    $sql="";
    if(@cmds){#we have new project records to add to the warehouse
        $sql .= join(';',@cmds);
        $sql .=";";
    }
    if(@updcmds){#We have updates to make
        $sql .= join(';',@updcmds);
        $sql .=";";
    }
    if($sql ne ""){
        $sth = $odsdbh->prepare($sql);
        $sth->execute;
        $sth->finish();
    }
}#end build_project_information

#------------------------------------------
sub build_expense_information {
    my($sdate,$edate)=@_;
    my($sql,$sth);
    &open_cp_dbconnection;
    my $dbfmt_sdate = $cpdbh->quote($sdate);
    my $dbfmt_edate = $cpdbh->quote($edate);
    my($m1,$d1,$y1)=split(/\//,$edate);
    my($m2,$d2,$y2)=split(/\//,$sdate);
    my $edatecomp = sprintf('%04d%02d%02d',$y1,$m1,$d1);
    my $sdatecomp = sprintf('%04d%02d%02d',$y2,$m2,$d2);
    my ($wghash)=&get_whse_workgroup_information();
    my ($rsrchash)=&get_whse_resource_information();
    my ($custhash)=&get_whse_customer_information();
    my ($projhash)=&get_whse_project_information();

    #Get Expense Detail information
    $sql = "
SELECT ex.expensedate
       ,convert(varchar(10),convert(date,DATEADD(wk, DATEDIFF(d, 0, ex.expensedate) / 7, 0)),101) as busweek
       ,c.name as customer_name
       ,e.name as engagement_name
       ,r.name as resource
	   ,wg.name as workgroup
       ,isnull(cast(er.expensereportnumber as varchar),'Not Submitted') as expensereportnumber
	   ,case ex.billable when 1 then 'Y' else 'N' end as billable
       ,p.name as project_name
       ,cat.name as category_name
       ,convert(varchar(250),isnull(typ.description,'')) as type_name
       ,convert(varchar(2000),isnull(ex.description,'')) as exp_descr
       ,convert(varchar(2000),isnull(er.comments,'')) as exp_rpt_comments
       ,convert(varchar(250),typ.description + iif(ex.description is not null,iif(ex.description <> '',' (' + ex.description + ')',''),'')) as long_descr
       ,'CP' as exp_source
       ,ex.quantity
       ,ex.unitprice
       ,ex.exchangerate
       ,ex.quantity * ex.unitprice * ex.exchangerate as ext_price
       ,ex.currencycode
       ,ex.quantity * ex.unitprice as orig_curr_amt
       ,ex.exchangerate
       ,ex.taxamount
       ,iif(ex.billable = 0,nbgl.glacode,bgl.glacode) as glacode
       ,ex.expensereportid
       ,ex.expenseid
       ,ex.cprowid as sort_key
       ,ex.customerid
       ,ex.engagementid
       ,ex.projectid
	   ,ex.ResourceId
	   ,ex.WorkgroupId
  FROM dbo.Expense ex
       left join expenseglas egla on ex.CategoryId = egla.id and ex.workgroupid = egla.workgroupid
       left join glacodes bgl on egla.BillableGLAId  = bgl.id
       left join glacodes nbgl on egla.NonBillableGLAId = nbgl.id
       left join dbo.customer c on ex.customerid = c.customerid
       left join dbo.engagement e on ex.engagementid = e.engagementid
       left join dbo.project p on ex.projectid = p.projectid
	   left join dbo.workgroup wg on ex.workgroupid = wg.WorkgroupId
       left join dbo.ExpenseCategory cat on ex.categoryid = cat.id
       left join dbo.expensetype typ on ex.expensetype = typ.ExpenseTypeId 
       left join dbo.resources r on ex.resourceid = r.resourceid
       left join dbo.expensereport er on ex.expensereportid = er.expensereportid
where ex.expensedate between $dbfmt_sdate and $dbfmt_edate
";

    $sth = $cpdbh->prepare($sql);
    if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to prepare expense build query","Query - $sql");}
    $sth->execute();
    if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to execute expense build query","Query - $sql");}
    my (@cols,@vals,%rowinfo,@insertsql)=();
    my $rowcount=0;
    while (my $info = $sth->fetchrow_hashref('NAME_lc')){
        $rowcount++;
        %rowinfo=();
        $rowinfo{resourceid}=$rsrchash->{cprsrcid}{$info->{resourceid}}{rsrcid};
        $rowinfo{workgroupid}=$wghash->{cpwgid}{$info->{workgroupid}}{wgid};

        if(defined($custhash->{cpcustid}{$info->{customerid}}{custid})){
            $rowinfo{customerid}=$custhash->{cpcustid}{$info->{customerid}}{custid}; 
        }
        if(defined($projhash->{cpprojid}{$info->{projectid}}{projid})){
            $rowinfo{projectid}=$projhash->{cpprojid}{$info->{projectid}}{projid};
        }

        $rowinfo{expensedate}=$cpdbh->quote($info->{expensedate});
        $rowinfo{businessweek}=$cpdbh->quote($info->{busweek});
        $rowinfo{projectname}=$cpdbh->quote($info->{project_name});
        $rowinfo{billable}=$cpdbh->quote($info->{billable});
        $rowinfo{expenseitemcomments}=$cpdbh->quote($info->{exp_descr});
        $rowinfo{expensereportnbr}=$cpdbh->quote($info->{expensereportnumber});
        $rowinfo{expensecategory}=$cpdbh->quote($info->{category_name});
        $rowinfo{cpexpenseid}=$cpdbh->quote($info->{expenseid});
        $rowinfo{expsrc}=$cpdbh->quote($info->{exp_source});
        $rowinfo{expensereportcomments}=$cpdbh->quote($info->{exp_rpt_comments});
        $rowinfo{currencycode}=$cpdbh->quote($info->{currencycode});

        $rowinfo{quantity}=$info->{quantity}*1;
        $rowinfo{unitprice}=$info->{unitprice}*1;
        $rowinfo{exchangerate}=$info->{exchangerate}*1;

        @cols=@vals=();
        foreach my $key(keys %rowinfo){
            push @cols,$key;
            push @vals,$rowinfo{$key}
        }
        push @insertsql,"insert into ".$ods_schema.".dbo.expenseinfo".$ods_tbl_suffix." (".join(',',@cols).") values (".join(',',@vals).")";
    }
    $sth->finish();
    #Start the actual DB updates
    $sql = "begin transaction;set xact_abort on;".
           "delete from ".$ods_schema.".dbo.expenseinfo".$ods_tbl_suffix." where expensedate between ".$dbfmt_sdate." and ".$dbfmt_edate.";".
            join(";\n",@insertsql).";".
           "commit transaction;";
    my $executecmds = 1;
    $odsdbh=&open_ODS_dbconnection();
    $sth = $odsdbh->prepare($sql);
    if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to prepare expense information update query","Query - $sql");}
    if($executecmds == 1){
        $sth->execute;
        if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to execute expense information update query","Query - $sql");}
    }
    $sth->finish();

        
}#end build_expense_information

#------------------------------------------
sub build_time_information {
    my($sdate,$edate)=@_;
    my($sql,$sth);
    &open_cp_dbconnection;
    my $dbfmt_sdate = $cpdbh->quote($sdate);
    my $dbfmt_edate = $cpdbh->quote($edate);
    my($m1,$d1,$y1)=split(/\//,$edate);
    my($m2,$d2,$y2)=split(/\//,$sdate);
    my $edatecomp = sprintf('%04d%02d%02d',$y1,$m1,$d1);
    my $sdatecomp = sprintf('%04d%02d%02d',$y2,$m2,$d2);
    my ($wghash)=&get_whse_workgroup_information();
    my ($rsrchash)=&get_whse_resource_information();
    my ($custhash)=&get_whse_customer_information();
    my ($projhash)=&get_whse_project_information();

    #Get Standard Time (non-project based)
    $sql = "
select t.timeid as time_entry_id,
       t.resourceid as sid,
       'StdTime' as time_source,
       0 as posted_rate,
       'C' as posting_state,
       r.lastname as lname,
       '--' as breakflag,
       0 as postbill,
       'OST' as cust,
       'Standard Task - ' + st.name as proj,
       '--' as task,
       '--' as task_type,
       convert(varchar(10),convert(date,DATEADD(wk, DATEDIFF(d, 0, t.datebooked) / 7, 0)),101) as busweek,
       convert(varchar(10),t.datebooked,101) as workdate,
       convert(varchar(10),t.datebooked,112) as workdate_comp,
       datepart(dw,t.datebooked) as dayofweek,
       t.createdon as create_datetime,
       case t.taskid
            when '6C9EF9F2-DAEF-11D2-882B-006097B596A6' then 10 -- Vacation => Paid Time Off
            when '6C9EF9F3-DAEF-11D2-882B-006097B596A6' then 10 -- Sick => Paid Time off
            when 'E3C782CC-A87A-4B90-A4BA-5B863B81D95B' then 16 -- Training (self) => Training
            when '504FC4DE-7529-4149-8192-6D2A51EBB52B' then 10 -- Other Paid Leave => Paid Time Off
			when 'B76CE1ED-48DE-496D-9662-2BE0AA9C8D74' then 10 -- Adoption => Paid Time Off
			when '60E50EC0-0E51-4992-A14D-1D336E41A053' then 10 -- Birthday => Paid Time Off
			when 'E449ED4F-C3A1-42A0-B725-2B096E3781DD' then 10 -- Bereavement => Paid Time Off
			when 'E96B3C49-3D82-4767-8EE1-459ED174EF7D' then 10 -- Jury Duty => Paid Time Off
			when 'C929C9D8-1D1A-4190-92C9-E3B8F8C4BD07' then 10 -- Military => Paid Time Off
			when '5C95A5C2-EACB-4D16-B2E0-5AF444B57A8E' then 10 -- Maternity => Paid Time Off
			when '67E4EC42-FA84-4AD8-AA3A-89F425726589' then 10 -- Paternity => Paid Time Off
            when '472E5F77-1047-4AC1-B458-23970D9E87E3' then 21 -- Unpaid Leave => Unpaid Leave
            when '3DBC00EF-DC05-49B5-96F5-D0B544CDA786' then  6 -- Training (apprvd) => Training
            when '4ACEF12A-D9DD-463C-B69D-F68560810DB7' then  7 -- OST Administrative => Administrative
            when '00403F26-86C4-4260-8881-CD26C07444E0' then 
                case when hol.val = 'Y' then 12  -- Logged as holiday on OST Holiday date
                     else 20 end                 -- Logged as holiday, but not on OST Holiday date so treat as PTO

            else 0 end as billing_type_id,
       case t.taskid
            when '6C9EF9F2-DAEF-11D2-882B-006097B596A6' then 'Paid Time Off'       -- Vacation
            when '6C9EF9F3-DAEF-11D2-882B-006097B596A6' then 'Paid Time Off'       -- Sick
            when 'E3C782CC-A87A-4B90-A4BA-5B863B81D95B' then 'Training (self)'     -- Training (self)
            when '504FC4DE-7529-4149-8192-6D2A51EBB52B' then 'Paid Time Off'       -- Paid Leave
			when 'B76CE1ED-48DE-496D-9662-2BE0AA9C8D74' then 'Paid Time Off' 	-- Adoption
			when '60E50EC0-0E51-4992-A14D-1D336E41A053' then 'Paid Time Off' 	-- Birthday
			when 'E449ED4F-C3A1-42A0-B725-2B096E3781DD' then 'Paid Time Off' 	-- Bereavement
			when 'E96B3C49-3D82-4767-8EE1-459ED174EF7D' then 'Paid Time Off' 	-- Jury Duty
			when 'C929C9D8-1D1A-4190-92C9-E3B8F8C4BD07' then 'Paid Time Off' 	-- Military
			when '5C95A5C2-EACB-4D16-B2E0-5AF444B57A8E' then 'Paid Time Off' 	-- Maternity
			when '67E4EC42-FA84-4AD8-AA3A-89F425726589' then 'Paid Time Off' 	-- Paternity
            when '472E5F77-1047-4AC1-B458-23970D9E87E3' then 'Unpaid Leave'        -- Unpaid Leave
            when '3DBC00EF-DC05-49B5-96F5-D0B544CDA786' then 'Training (apprvd)'   -- Training (approved)
            when '4ACEF12A-D9DD-463C-B69D-F68560810DB7' then 'Admin'               -- OST Administrative
            when '00403F26-86C4-4260-8881-CD26C07444E0' then 
                case when hol.val = 'Y' then 'Holiday'  -- Logged as holiday on OST Holiday date
                     else 'Holiday Alt (PTO)' end       -- Logged as holiday, but not on OST Holiday date so treat as PTO

            else 'Unknown' end as billtype,
       cost_rate = isnull((select top 1 r1.hourlycostrate  
                    from resourcerate r1
                   where r1.resourceid = t.resourceid
                     and r1.effectivedate <= t.DateBooked
                   order by r1.effectivedate desc
                 ),0),
       t.regularhours + t.overtimehours as postedhours,
       t.regularhours + t.overtimehours as hours,
       r.fullname as staff,
       t.workgroupid, w.name as workgroup,
       convert(varchar(10),(select top 1 effectivedate from resourcerate where active=1 and ResourceId = t.resourceid and effectivedate <= t.datebooked order by effectivedate desc),101) as rsrccosteffdate,
       (select top 1 hourlycostrate from resourcerate where active=1 and ResourceId = t.resourceid and effectivedate <= t.datebooked order by effectivedate desc) as rsrccostrate
       ,convert(varchar(2000),t.description) as notes
from StandardTaskTime t
     left join StandardTask st on t.TaskId = st.TaskId
     left join Resources r on t.resourceid = r.resourceid
     left join Workgroup w on t.workgroupid = w.workgroupid
     left join (select convert(varchar(10),nonworkingdate,101) as hdate, 
                       description, 
                       'Y' as val 
                  from nonworkingdaysettings 
                 where nonworkingdate between $dbfmt_sdate and $dbfmt_edate
                ) hol on convert(varchar(10),t.datebooked,101) = hol.hdate
where t.datebooked between $dbfmt_sdate and $dbfmt_edate
";
    $sth = $cpdbh->prepare($sql);
    if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to prepare standard time build query","Query - $sql");}
    $sth->execute();
    if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to execute standard time build query","Query - $sql");}
    my (@cols,@vals,%rowinfo,@insertsql)=();
    my $rowcount=0;
    while (my $info = $sth->fetchrow_hashref('NAME_lc')){
        $rowcount++;
        %rowinfo=();
        $rowinfo{workgroupid}=$wghash->{cpwgid}{$info->{workgroupid}}{wgid};
        $rowinfo{resourceid}=$rsrchash->{cprsrcid}{$info->{sid}}{rsrcid};
        $rowinfo{customerid}=$custhash->{ostid}{custid};
        $rowinfo{primaryfunctionid}=$rsrchash->{cprsrcid}{$info->{sid}}{currentfid};

        $rowinfo{projectid}=0;
        $rowinfo{timesrc}=$cpdbh->quote($info->{time_source});
        $rowinfo{timesrcid}=$cpdbh->quote($info->{time_entry_id});
        $rowinfo{businessweek}=$cpdbh->quote($info->{busweek});
        $rowinfo{workdate}=$cpdbh->quote($info->{workdate});
        $rowinfo{src_create_datetime}=$cpdbh->quote($info->{create_datetime});
        $rowinfo{timetype}=$cpdbh->quote($info->{billtype});
        $rowinfo{timetypeid}=$info->{billing_type_id};
        $rowinfo{hours}=$info->{hours};
        $rowinfo{ratecardcost}=$info->{cost_rate}*1;
        $rowinfo{rsrccostrate}=$info->{rsrccostrate}*1;
        $rowinfo{rsrccosteffdate}=$cpdbh->quote($info->{rsrccosteffdate});
        $rowinfo{notes}=$cpdbh->quote($info->{notes});
        @cols=@vals=();
        foreach my $key(keys %rowinfo){
            push @cols,$key;
            push @vals,$rowinfo{$key}
        }
        push @insertsql,"insert into ".$ods_schema.".dbo.timeinfo".$ods_tbl_suffix." (".join(',',@cols).") values (".join(',',@vals).")";
    }
    $sth->finish();

    $sql = "
-- Get project time entries
select t.timeid as time_entry_id,
       t.customerid,
       r.resourceid as sid,
       'ProjTime' as time_source,
       t.BillingRate as posted_rate, 
       t.CostRate as cost_rate,
       'C' as posting_state,
       r.lastname as lname,
       '--' as breakflag,
       t.billable as postbill,
       tsk.billable as taskbill,
       c.name as cust, 
       p.name as proj,
       p.projectid as projectid,
       tsk.name as task,
       cdet.numericequivalent as project_reporting_type_id,  -- Project Reporting Type ID in CP
       cdet.description as project_reporting_type,           -- Project Reporting Type
       prdet.description as project_classification,          -- Project Classification
       tcdet.numericequivalent as task_type_id,              -- Task Type ID in CP
       p.cpprojecttype as project_classification_id,         -- Project Classification ID in CP
       tcdet.description as task_type,
       convert(varchar(10),dateadd(dd, ((datediff(dd, '17530107', t.timedate)/7)*7)+7, '17530107'),101) as busweek_old,
       convert(varchar(10),convert(date,DATEADD(wk, DATEDIFF(d, 0, t.timedate) / 7, 0)),101) as busweek,
       convert(varchar(10),t.timedate,101) as workdate,
       convert(varchar(10),t.timedate,112) as workdate_comp,
       datepart(dw,t.timedate) as dayofweek,
       t.createdon as create_datetime,
       case when t.billable = 1 then 1                                        -- (Billable       - Time)
            when tcdet.numericequivalent = 2 then 8                           -- (Travel         - Task)
            when cdet.numericequivalent = 2 then                              -- ***(Project Reporting Type = Pre-Sales)***
                 case when tcdet2.numericequivalent = 1 then 3                -- (Expect to Bill - Task)
                  when tcdet3.numericequivalent = 1 then 3 
                 else 2 end                                                   -- (Pre-Sales      - Project)
            when prdet.description = 'Managed Services' then                  -- ***(Project Classification = Managed Services)***
                 case when tsk.Billable = 1 then 1                            -- (Billable       - Task is Billable)
                      when tsk.FixedFee = 1 then 9                            -- (Mgd Svcs       - Task is FixedFee)
                 else 5 end                                                   -- (Non-Billable   - Default)
            when cdet.numericequivalent = 3 then                              -- ***(Project Reporting Type = Managed Services)***
                 case when tsk.Billable = 1 then 1                            -- (Billable       - Task is Billable)
                      when tsk.FixedFee = 1 then 9                            -- (Mgd Svcs       - Task is FixedFee)
                 else 5 end                                                   -- (Non-Billable   - Default)
            when cdetutilelig.numericequivalent = 1 then 11                   -- (Utilization Eligible = Yes)
            else 5 end                                                        -- (Non-Billable   - Default)
            as billing_type_id,
       case when t.billable = 1 then 'Billable'                                 -- (Time)
            when tcdet.numericequivalent = 2 then 'Travel'                      -- (Task)
            when cdet.numericequivalent = 2 then                                -- ***(Project Reporting Type = Pre-Sales)***
                 case when tcdet2.numericequivalent = 1 then 'Expect To Bill'   -- (Task)
                 WHEN tcdet3.numericequivalent = 1 THEN 'Expect To Bill'
                 else 'Pre-Sales' end                                           -- (Project)
            when prdet.description = 'Managed Services' then                    -- (Project Classification = Managed Services)
                 case when tsk.Billable = 1 then 'Billable'                     -- (Task is Billable)
                      when tsk.FixedFee = 1 then 'Managed Services'             -- (Task is FixedFee)
                 else 'Non-Billable' end                                        -- (Default)
            when cdet.numericequivalent = 3 then                                -- (Project Reporting Type = Managed Services)
                 case when tsk.Billable = 1 then 'Billable'                     -- (Task is Billable)
                      when tsk.FixedFee = 1 then 'Managed Services'             -- (Task is FixedFee)
                 else 'Non-Billable' end                                        -- (Default)
            when cdetutilelig.numericequivalent = 1 then 'Non Bill Utiliz Elig' -- (Utilization Eligible = Yes)
            else 'Non-Billable' end                                             -- (Default)
            as billtype,
       t.regularhours + t.overtimehours as postedhours,
       t.regularhours + t.overtimehours as hours,
       r.fullname as staff,
       t.workgroupid, w.name as workgroup,
       convert(varchar(10),(select top 1 effectivedate from resourcerate where active=1 and ResourceId = t.resourceid and effectivedate <= t.timedate order by effectivedate desc),101) as rsrccosteffdate,
       (select top 1 hourlycostrate from resourcerate where active=1 and ResourceId = t.resourceid and effectivedate <= t.timedate order by effectivedate desc) as rsrccostrate
       ,convert(varchar(2000),t.description) as notes
from Time t
     left join customer c on t.customerid = c.customerid
     left join Resources r on t.resourceid = r.resourceid
     left join project p on t.projectid = p.projectid
     left join Tasks tsk on t.taskid = tsk.taskid
      left join dbo.TaskAssignment TA ON TA.TaskId = tsk.TaskId AND TA.ResourceId = R.ResourceId AND tA.Deleted = 0
     left join Workgroup w on t.workgroupid = w.workgroupid
     left join dbo.udfcode ucode on p.projectid = ucode.entityid and ucode.itemname = 'ProjectCode506'
     left join dbo.codedetail cdet on ucode.udfcode = cdet.codedetail
     left join dbo.udfcode tucode on tsk.taskid = tucode.entityid and tucode.itemname = 'TaskCode500'
     left join dbo.codedetail tcdet on tucode.udfcode = tcdet.codedetail
     left join dbo.udfcode tucode2 on tsk.taskid = tucode2.entityid and tucode2.itemname = 'TaskCode501'
     left join dbo.codedetail tcdet2 on tucode2.udfcode = tcdet2.codedetail
     	LEFT join dbo.udfcode tucode3 on ta.TaskAssignmentId = tucode3.entityid and tucode3.itemname = 'TaskCode501'
     left join dbo.codedetail tcdet3 on tucode3.udfcode = tcdet3.codedetail
     left join dbo.codedetail prdet on p.cpprojecttype = prdet.codedetail
     left join dbo.udfcode ucodeutilelig on p.projectid = ucodeutilelig.entityid and ucodeutilelig.itemname = 'ProjectCode509'
     left join dbo.codedetail cdetutilelig on ucodeutilelig.udfcode = cdetutilelig.codedetail
where t.timedate between $dbfmt_sdate and $dbfmt_edate
";
    $sth = $cpdbh->prepare($sql);
    if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to prepare project time build query","Query - $sql");}
    $sth->execute();
    if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to execute project time build query","Query - $sql");}

    while (my $info = $sth->fetchrow_hashref('NAME_lc')){
        %rowinfo=();
        $rowinfo{workgroupid}=$wghash->{cpwgid}{$info->{workgroupid}}{wgid};
        $rowinfo{resourceid}=$rsrchash->{cprsrcid}{$info->{sid}}{rsrcid};
        $rowinfo{primaryfunctionid}=$rsrchash->{cprsrcid}{$info->{sid}}{currentfid};

        if(defined($custhash->{cpcustid}{$info->{customerid}}{custid})){
            $rowinfo{customerid}=$custhash->{cpcustid}{$info->{customerid}}{custid}; #Problem .... Standard has "OST" customer, no customerid
        }
        if(defined($projhash->{cpprojid}{$info->{projectid}}{projid})){
            $rowinfo{projectid}=$projhash->{cpprojid}{$info->{projectid}}{projid};
        }
        $rowinfo{timesrc}=$cpdbh->quote($info->{time_source});
        $rowinfo{timesrcid}=$cpdbh->quote($info->{time_entry_id});
        $rowinfo{businessweek}=$cpdbh->quote($info->{busweek});
        $rowinfo{workdate}=$cpdbh->quote($info->{workdate});
        $rowinfo{src_create_datetime}=$cpdbh->quote($info->{create_datetime});
        $rowinfo{timetype}=$cpdbh->quote($info->{billtype});
        $rowinfo{projectname}=$cpdbh->quote($info->{proj});
        $rowinfo{taskname}=$cpdbh->quote($info->{task});
        $rowinfo{timetypeid}=$info->{billing_type_id};
        $rowinfo{cptimebillflag}=$info->{postbill};
        $rowinfo{cptaskbillflag}=$info->{taskbill};
        $rowinfo{hours}=$info->{hours}*1;
        $rowinfo{billrate}=$info->{posted_rate}*1;
        $rowinfo{ratecardcost}=$info->{cost_rate}*1;
        $rowinfo{rsrccosteffdate}=$cpdbh->quote($info->{rsrccosteffdate});
        $rowinfo{rsrccostrate}=$info->{rsrccostrate}*1;
        $rowinfo{rsrccosteffdate}=$cpdbh->quote($info->{rsrccosteffdate});
        $rowinfo{notes}=$cpdbh->quote($info->{notes});
        $rowinfo{project_reporting_type}=$cpdbh->quote($info->{project_reporting_type});
        $rowinfo{project_classification}=$cpdbh->quote($info->{project_classification});
        $rowinfo{project_reporting_type_id}=$info->{project_reporting_type_id}*1;
        $rowinfo{task_type_id}=$info->{task_type_id}*1;
        $rowinfo{project_classification_id}=$cpdbh->quote($info->{project_classification_id});
        @cols=@vals=();
        foreach my $key(keys %rowinfo){
            push @cols,$key;
            push @vals,$rowinfo{$key}
        }
        push @insertsql,"insert into ".$ods_schema.".dbo.timeinfo".$ods_tbl_suffix." (".join(',',@cols).") values (".join(',',@vals).")";
    }
    $sth->finish();

    #Start the actual DB updates
    $sql = "begin transaction;set xact_abort on;".
           "delete from ".$ods_schema.".dbo.timeinfo".$ods_tbl_suffix." where workdate between ".$dbfmt_sdate." and ".$dbfmt_edate.";".
            join(";\n",@insertsql).";".
           "commit transaction;";
    my $executecmds = 1;
    $odsdbh=&open_ODS_dbconnection();
    $sth = $odsdbh->prepare($sql);
    if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to prepare time information update query","Query - $sql");}
    if($executecmds == 1){
        $sth->execute;
        if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to execute time information update query","Query - $sql");}
    }
    $sth->finish();
}#end build_time_information

#------------------------------------------
sub get_time_recs_for_utiliz_build {
    my($sdate,$edate)=@_;
    my($sql,$sth);
    $odsdbh=&open_ODS_dbconnection();
    my $dbfmt_sdate = $odsdbh->quote($sdate);
    my $dbfmt_edate = $odsdbh->quote($edate);
    $sql = "
select t.timesrc as time_source,
	r.CPResourceID as sid,
	r.ResourceName as staff,
	w.WorkgroupName as workgroup,
	w.cpworkgroupid as workgroupid,
	t.TimeSrcID as time_entry_id,
	t.BusinessWeek as busweek,
	t.workdate as workdate,
	t.timetype as billtype,
	t.TimeTypeID as billing_type_id,
	t.hours as hours,
    case when datename(dw,t.workdate) in ('Saturday','Sunday') then 0 else 1 end as weekdayflag
from ".$ods_schema.".dbo.timeinfo".$ods_tbl_suffix." t
     left join ".$ods_schema.".dbo.resourceinfo".$ods_tbl_suffix." r on t.resourceid = r.resourceid
	 left join ".$ods_schema.".dbo.workgroupinfo".$ods_tbl_suffix." w on t.workgroupid = w.workgroupid
where t.workdate between $dbfmt_sdate and $dbfmt_edate
";
    $sth = $odsdbh->prepare($sql);
    if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to prepare time_recs_for_utiliz query","Query - $sql");}
    $sth->execute();
    if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to execute time_recs_for_utiliz query","Query - $sql");}
    my $info = $sth->fetchall_hashref('time_entry_id');
    $sth->finish();
    return($info);
    
}#end get_time_recs_for_utiliz_build

#------------------------------------------
sub build_utilization_information { #New version as of 7/27/2017
    my($sdate,$edate)=@_;
    my ($thash)=&get_time_recs_for_utiliz_build($sdate,$edate);
    my $cpholidays=&get_cpholidays();#Have to do this before starting the main query to avoid error (M$ can't handle multiple connections)
    my ($wghash)=&get_whse_workgroup_information();
    my ($rsrchash)=&get_whse_resource_information();
    my ($pfhash)=&get_whse_primary_function_information();

    #-----------------------------------------------------------------
    #Main loop to build Utilization record information
    #-----------------------------------------------------------------
    my (@cols,@vals,%rowinfo,@insertsql)=();
    my $utilizrsrc = ();
    my $rowcount=0;my %timesrc=();my @buildprocnotes=();
    
    my($info);
    foreach my $key(keys %{$thash}){
        $info=$thash->{$key};
        $rowcount++;$timesrc{$info->{time_source}}++;
        @buildprocnotes=();
        $utilizrsrc->{$rsrchash->{cprsrcid}{$info->{sid}}{rsrcid}}{cprsrcid}=$info->{sid};# $utilizrsrc->{whsrsrcid}{cprsrcid}=CP_rsrcid
        $utilizrsrc->{$rsrchash->{cprsrcid}{$info->{sid}}{rsrcid}}{cprsrcid}=$info->{sid};# $utilizrsrc->{whswrkgrpid}{cpwrkgrpid}=CP_wrkgrpid
        %rowinfo=();
        $rowinfo{resourcename}=$cpdbh->quote($info->{staff});
        $rowinfo{workgroupname}=$cpdbh->quote($info->{workgroup});
        $rowinfo{workgroupid}=$wghash->{cpwgid}{$info->{workgroupid}}{wgid};
        $rowinfo{resourceid}=$rsrchash->{cprsrcid}{$info->{sid}}{rsrcid};

        $rowinfo{primaryfunctionid}=$rsrchash->{cprsrcid}{$info->{sid}}{currentfid}||0;
        $rowinfo{primaryfunctionname}=$odsdbh->quote($pfhash->{fid}{$rowinfo{primaryfunctionid}}{name});

        $rowinfo{cp_timesrc}=$cpdbh->quote($info->{time_source});
        $rowinfo{cp_timeid}=$cpdbh->quote($info->{time_entry_id});
        $rowinfo{businessweek}=$cpdbh->quote($info->{busweek});
        $rowinfo{workdate}=$cpdbh->quote($info->{workdate});
        $rowinfo{billingtype}=$cpdbh->quote($info->{billtype});
        $rowinfo{billingtypeid}=$info->{billing_type_id};
        $rowinfo{totalhours}=$info->{hours}*1;

           if($info->{billing_type_id}== 1){$rowinfo{billablehours}=$info->{hours}*1;}
        elsif($info->{billing_type_id}== 2){$rowinfo{presaleshours}=$info->{hours}*1;}
        elsif($info->{billing_type_id}== 3){$rowinfo{presalesexpecttobill}=$info->{hours}*1;}
        elsif($info->{billing_type_id}== 5){$rowinfo{nonbillhours}=$info->{hours}*1;}
        elsif($info->{billing_type_id}== 6){$rowinfo{trngapprvdhours}=$info->{hours}*1;}
        elsif($info->{billing_type_id}== 7){$rowinfo{adminhours}=$info->{hours}*1;}
        elsif($info->{billing_type_id}== 8){$rowinfo{travelhours}=$info->{hours}*1;}
        elsif($info->{billing_type_id}== 9){$rowinfo{mgdshours}=$info->{hours}*1;}
        elsif($info->{billing_type_id}==10){
            if($info->{weekdayflag}==1){$rowinfo{pto_hours}=$info->{hours}*1;}
            else{push @buildprocnotes,sprintf('%.2f',$info->{hours}*1). " hours of PTO entered for a weekend date.  Ignoring for utilization purposes";
                 $rowinfo{totalhours}=0;
                }
        }
        elsif($info->{billing_type_id}==11){$rowinfo{nonbillutilizelighours}=$info->{hours}*1;}
        elsif($info->{billing_type_id}==12){$rowinfo{holidayhours}=$info->{hours}*1;}
        elsif($info->{billing_type_id}==16){$rowinfo{trngselfhours}=$info->{hours}*1;}
        elsif($info->{billing_type_id}==20){
            $rowinfo{holidayalthours}=$info->{hours}*1;
            $utilizrsrc->{$rsrchash->{cprsrcid}{$info->{sid}}{rsrcid}}{altholiday}{$info->{workdate}}="Y";
        }
        elsif($info->{billing_type_id}==21){
            if($info->{weekdayflag}==1){$rowinfo{unpaidleavehours}=$info->{hours}*1;}
            else{push @buildprocnotes,sprintf('%.2f',$info->{hours}*1). " hours of undpaid leave entered for a weekend date.  Ignoring for utilization purposes";
                 $rowinfo{totalhours}=0;
                }
        }
        $rowinfo{buildprocnotes}=$cpdbh->quote(join(' || ',@buildprocnotes));

        @cols=@vals=();
        foreach my $key(keys %rowinfo){
            push @cols,$key;
            push @vals,$rowinfo{$key}
        }
        push @insertsql,"insert into ".$ods_schema.".dbo.utilizationinfo".$ods_tbl_suffix." (".join(',',@cols).") values (".join(',',@vals).")";
    }
    #Start the actual DB updates
    $odsdbh=&open_ODS_dbconnection();
    my $dbfmt_sdate = $odsdbh->quote($sdate);
    my $dbfmt_edate = $odsdbh->quote($edate);
    $sql = "begin transaction;set xact_abort on;".
           "delete from ".$ods_schema.".dbo.utilizationinfo".$ods_tbl_suffix." where workdate between ".$dbfmt_sdate." and ".$dbfmt_edate.";".
            join(";\n",@insertsql).";".
           "commit transaction;";
    my $executecmds = 1;
    $odsdbh=&open_ODS_dbconnection();
    $sth = $odsdbh->prepare($sql);
    if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to prepare Utilization Info update query","Query - $sql");}
    if($executecmds == 1){
        $sth->execute;
        if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to execute Utilization Info update query","Query - $sql");}
    }
    $sth->finish();
   
}#end build_utilization_information

#------------------------------------------
sub build_availability_info{
    my($sdate,$edate)=@_;
    my($sql,$sth);
    my $odsdbh=&open_ODS_dbconnection();
    my $dbfmt_sdate = $odsdbh->quote($sdate);
    my $dbfmt_edate = $odsdbh->quote($edate);
    my $cpholidays=&get_cpholidays();#Have to do this before starting the main query to avoid error (M$ can't handle multiple connections)
    my $rsrcwghist=&get_cp_resource_wg_history_dates();
    my $rsrcinfo=&get_whse_resource_information($sdate,$edate);
    my $wginfo=&get_whse_workgroup_information();
    my $pfinfo=&get_whse_primary_function_information();

    my @availsql=();
    my($m,$m1,$m2,$d,$d1,$d2,$y,$y1,$y2,$dow,$chkdate,$chkdate1,$sm,$sd,$sy,$em,$ed,$ey);
    ($m1,$d1,$y1)=split(/\//,$sdate);
    ($m2,$d2,$y2)=split(/\//,$edate);
    my $halfday = "1/2";
    my $numdays=Delta_Days($y1,$m1,$d1,$y2,$m2,$d2);
    my($rsrcid,$wgid,%rowinfo,@cols,@vals);
    my($availdays,$wgavaildays);
    my($wgavailhours,$stdholidayhours,$altholidayhours,$ptohours,$unpaidleavehours,$apprvdtrnghours,$billhours,$presaleshours,$nonbillhours,$adminhours,$travelhours,$mgdshours,$trngselfhours);
    my $templn=0;
    my $errcnt=0;
    for(my $dcnt=0;$dcnt <= $numdays;$dcnt++){
        ($y,$m,$d)=Add_Delta_Days($y1,$m1,$d1,$dcnt);
        $dow=Day_of_Week($y,$m,$d);
        next unless ($dow >= 1 and $dow <= 5);#Only run loop/check for Mon-Fri
        $chkdate=sprintf('%.2d/%.2d/%.4d',$m,$d,$y);
        $chkdate1=sprintf('%.4d%.2d%.4d',$y,$m,$d);

        #Next, loop through the resources
        foreach my $cprsrcid(keys %{$rsrcwghist}){
            $rsrcid=$rsrcinfo->{cprsrcid}{$cprsrcid}{rsrcid};

            #Next, for each defined workgroup
            foreach my $cpwgid(keys %{$rsrcwghist->{$cprsrcid}}){
               $wgid=$wginfo->{cpwgid}{$cpwgid}{wgid};
                next unless($wgid);#Skip unless resource has ever been a member of this group
                $availdays=$wgavaildays=0;#start assuming zero avail for the date/workgroup
                $wgavailhours=$stdholidayhours=$altholidayhours=$ptohours=$unpaidleavehours=$apprvdtrnghours=$billhours=$presaleshours=$nonbillhours=$adminhours=$travelhours=$mgdshours=$trngselfhours=-1;
                    
                #Determine available hours
                foreach my $ln(keys %{$rsrcwghist->{$cprsrcid}{$cpwgid}}){
                    if($chkdate1 >= sprintf('%.4d%.2d%.4d',split(/\-/,$rsrcwghist->{$cprsrcid}{$cpwgid}{$ln}{sdate}))  and 
                       $chkdate1 <  sprintf('%.4d%.2d%.4d',split(/\-/,$rsrcwghist->{$cprsrcid}{$cpwgid}{$ln}{edate}))
                       ){
                        $availdays=$wgavaildays = 1;
                        $stdholidayhours=0;
                        }
                }
                next unless $wgavaildays > 0;#Resource not associated with this group on this date

                #Next check for any OST holidays
                if(defined($cpholidays->{$chkdate})){ #This is an official OST holiday
                    $stdholidayhours = 8;#Most holidays are all day
                    if($cpholidays->{$chkdate}{description} =~ /$halfday/){ #This is a 1/2 day official OST holiday
                        $stdholidayhours = 4;
                    }
                }
                
                #Next check for any time entry based hours
                $billhours=$rsrcinfo->{timeinfo}{$rsrcid}{$chkdate}{billable}||0;$billhours=8 if $billhours > 8;
                $presaleshours=$rsrcinfo->{timeinfo}{$rsrcid}{$chkdate}{presales}||0;$presaleshours=8 if $presaleshours > 8;
                $nonbillhours=$rsrcinfo->{timeinfo}{$rsrcid}{$chkdate}{nonbill}||0;$nonbillhours=8 if $nonbillhours > 8;
                $apprvdtrnghours=$rsrcinfo->{timeinfo}{$rsrcid}{$chkdate}{trngapproved}||0;$apprvdtrnghours = 8 if $apprvdtrnghours > 8;
                $adminhours=$rsrcinfo->{timeinfo}{$rsrcid}{$chkdate}{admin}||0;$adminhours=8 if $adminhours > 8;
                $travelhours=$rsrcinfo->{timeinfo}{$rsrcid}{$chkdate}{travel}||0;$travelhours=8 if $travelhours > 8;
                $mgdshours=$rsrcinfo->{timeinfo}{$rsrcid}{$chkdate}{mgds}||0;$mgdshours=8 if $mgdshours > 8;
                $ptohours=$rsrcinfo->{timeinfo}{$rsrcid}{$chkdate}{pto}||0;$ptohours=8 if $ptohours > 8;
                $trngselfhours=$rsrcinfo->{timeinfo}{$rsrcid}{$chkdate}{trngself}||0;$trngselfhours=8 if $trngselfhours > 8;
                $altholidayhours=$rsrcinfo->{timeinfo}{$rsrcid}{$chkdate}{altholiday_hours}||0;$altholidayhours = 8 if $altholidayhours > 8;
                $unpaidleavehours=$rsrcinfo->{timeinfo}{$rsrcid}{$chkdate}{unpaidleave}||0;$unpaidleavehours=8 if $unpaidleavehours > 8;

                    
                #Last, Add the rows to the database
                %rowinfo=();
                $rowinfo{resourceid}=$rsrcid;
                $rowinfo{workgroupid}=$wgid;
                $rowinfo{workdate}=$cpdbh->quote($chkdate);

                $rowinfo{workgroupid}=$wgid;
                $rowinfo{primaryfunctionid}=$rsrcinfo->{rsrcid}{$rsrcid}{currentfid};

                $rowinfo{wgavailhours}=$wgavaildays*8;
                $rowinfo{stdholidayhours}=$stdholidayhours;
                $rowinfo{altholidayhours}=$altholidayhours;
                $rowinfo{apprvdtrnghours}=$apprvdtrnghours;
                $rowinfo{ptohours}=$ptohours;
                $rowinfo{unpaidleavehours}=$unpaidleavehours;

                $rowinfo{billablehours}=$billhours*1;
                $rowinfo{presaleshours}=$presaleshours*1;
                $rowinfo{nonbillhours}=$nonbillhours*1;
                $rowinfo{adminhours}=$adminhours*1;
                $rowinfo{travelhours}=$travelhours*1;
                $rowinfo{mgdshours}=$mgdshours*1;
                $rowinfo{selftrnghours}=$trngselfhours*1;

                @cols=@vals=();
                foreach my $key(keys %rowinfo){
                    push @cols,$key;
                    push @vals,$rowinfo{$key}
                }
                push @availsql,"insert into ".$ods_schema.".dbo.resourceavailability".$ods_tbl_suffix." (".join(',',@cols).") values (".join(',',@vals).")";

            }#end workgroup loop
        }#end resource loop
    }#end of date range loop

    #Start the actual DB updates
    $sql = "begin transaction;set xact_abort on;".
           "delete from ".$ods_schema.".dbo.resourceavailability".$ods_tbl_suffix." where workdate between ".$dbfmt_sdate." and ".$dbfmt_edate.";".
            join(";\n",@availsql).";".
           "commit transaction;";
    my $executecmds = 1;
    $odsdbh=&open_ODS_dbconnection();
    $sth = $odsdbh->prepare($sql);
    if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to prepare Availability Info update query","Query - $sql");}
    if($executecmds == 1){
        $sth->execute;
        if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to execute Availability Info update query","Query - $sql");}
    }
    $sth->finish();

    print ".... ".@availsql." Records processed for Resource Availility information ...\n";
}#end build_availability_info

#------------------------------------------
sub get_cp_resource_wg_history_dates{
    my($list)=@_;
    my @rlist=();
    &open_cp_dbconnection;
    foreach my $id(split(/\|/,$list)){push @rlist,$cpdbh->quote($id);}
    my $where="";
    if(@rlist){$where = "where wh.resourceid in (".join(',',@rlist).")";}
#     my $sql="
# select convert(varchar(50),isnull(wg.name,'')) as workgroup,
#        wh.workgroupid,
#        convert(varchar(50),r.fullname) as fullname,
#        wh.resourceid,
#        wh.workgrouphistorymembertype,
#        convert(date,wh.effectivedate) as eff_date,
#        convert(date,ca1.enddate) as enddate, 
#        convert(varchar(10),wh.effectivedate,101) as eff_date_fmtd,
#        convert(varchar(10),ca1.enddate,101) as enddate_fmtd,
#        wh.createdon,
#        wh.historyid,
#  from WorkgroupHistoryMember wh
#       left join workgroup wg on wh.workgroupid = wg.workgroupid
#       left join resources r on wh.resourceid = r.resourceid
#       cross apply (select isnull(wh.enddate,dateadd(year,5,getdate())) as enddate) as ca1
# --where wh.resourceid = '90F4019D-14C1-4957-B48C-72D615DFA7BA'
# $where
# order by r.fullname asc, wg.name asc,wh.effectivedate desc
# ";

    my $sql = "
    SELECT v1.workgroup, v1.WorkgroupId, v1.fullname, v1.ResourceId, v1.EffectiveDate, v1.WorkGroupHistoryMemberType, v1.eff_date, 
        ISNULL(v1.CalculatedEndDate, v1.EndDate) AS enddate,
        v1.eff_date_fmtd,
        CONVERT(VARCHAR(10),ISNULL(v1.CalculatedEndDate, v1.EndDate), 101) AS enddate_fmtd,
        v1.CreatedOn,
        v1.HistoryId
    FROM 
    (
        SELECT 	
            v.*,
            CASE WHEN v.EndDate IS NULL AND (SELECT COUNT(*) FROM WorkgroupHistoryMember whm WHERE whm.ResourceId = v.ResourceId AND whm.EndDate IS NULL) > 1 AND v.WorkgroupId <> '00000000-0000-0000-0000-000000000000'
                THEN 
                    (SELECT TOP 1 EffectiveDate FROM WorkgroupHistoryMember whm WHERE whm.ResourceId = v.ResourceId AND whm.EndDate IS NULL AND whm.WorkgroupId = '00000000-0000-0000-0000-000000000000' ORDER BY EffectiveDate DESC)
                ELSE 
                    ISNULL(v.EndDate, DATEADD(YEAR, 5, GETDATE()))
            END AS CalculatedEndDate 
        FROM
        (
            SELECT CONVERT(VARCHAR(50), ISNULL(wg.NAME, '')) AS workgroup
                ,wh.workgroupid
                ,CONVERT(VARCHAR(50), r.fullname) AS fullname
                ,wh.resourceid
                ,wh.EffectiveDate
                ,wh.workgrouphistorymembertype
                ,CONVERT(DATE, wh.effectivedate) AS eff_date
                ,wh.enddate
                --,convert(date,ISNULL(wh.enddate, dateadd(year,5,getdate()))) as enddate, 
                ,CONVERT(VARCHAR(10), wh.effectivedate, 101) AS eff_date_fmtd
                --,convert(varchar(10),ISNULL(wh.enddate, dateadd(year,5,getdate())),101) as enddate_fmtd,
                ,wh.createdon
                ,wh.historyid
            FROM WorkgroupHistoryMember wh
            LEFT JOIN workgroup wg ON wh.workgroupid = wg.workgroupid
            LEFT JOIN resources r ON wh.resourceid = r.resourceid
            $where
        ) v
    ) v1
            ORDER BY v1.fullname ASC
                ,v1.workgroup ASC
                ,v1.effectivedate DESC

        ";
		
    my $sth = $cpdbh->prepare($sql);
    $sth->execute();if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to execute CP Resource WG history query","Query - $sql");}
    my $info=();
    my $rowcnt=0;
    my $daycnt=0;
    while(my $rec = $sth->fetchrow_hashref('NAME_lc')){
        $rowcnt++;
        $info->{$rec->{resourceid}}{$rec->{workgroupid}}{$rowcnt}{sdate}=$rec->{eff_date}; #yyyy-mm-dd
        $info->{$rec->{resourceid}}{$rec->{workgroupid}}{$rowcnt}{sdate_mdy}=$rec->{eff_date_fmtd}; #mm/dd/yyyy
        $info->{$rec->{resourceid}}{$rec->{workgroupid}}{$rowcnt}{edate}=$rec->{enddate}; #yyyy-mm-dd
        $info->{$rec->{resourceid}}{$rec->{workgroupid}}{$rowcnt}{edate_mdy}=$rec->{enddate_fmtd}; #mm/dd/yyyy
        $info->{$rec->{resourceid}}{$rec->{workgroupid}}{$rowcnt}{resource}=$rec->{fullname};
        $info->{$rec->{resourceid}}{$rec->{workgroupid}}{$rowcnt}{workgroup}=$rec->{workgroup};
    }
    $sth->finish();
    return($info);
}#end get_cp_resource_wg_history_dates

#------------------------------------------
sub build_workdate_info{
    my($sdate,$edate)=@_;

    my($sql,$sth);
    my $odsdbh=&open_ODS_dbconnection();
    my $dbfmt_sdate = $odsdbh->quote($sdate);
    my $dbfmt_edate = $odsdbh->quote($edate);

    my($m,$m1,$m2,$d,$d1,$d2,$y,$y1,$y2,$dow,$chkdate,$availdays,$y3,$m3,$d3);
    ($m1,$d1,$y1)=split(/\//,$sdate);
    ($m2,$d2,$y2)=split(/\//,$edate);
    my (%rowinfo,@cols,@vals,$workday,$busweek);
    my @sqlcmds=();
    my $numdays=Delta_Days($y1,$m1,$d1,$y2,$m2,$d2);
    for(my $dcnt=0;$dcnt <= $numdays;$dcnt++){
        ($y,$m,$d)=Add_Delta_Days($y1,$m1,$d1,$dcnt);
        ($y3,$m3,$d3)=Add_Delta_Days($y,$m,$d,1 - Day_of_Week($y,$m,$d));
        $busweek=sprintf('%.2d/%.2d/%.4d',$m3,$d3,$y3);
        $workday=sprintf('%.2d/%.2d/%.4d',$m,$d,$y);
        %rowinfo=();
        $rowinfo{busweek}=$cpdbh->quote($busweek);
        $rowinfo{workdate}=$cpdbh->quote($workday);
        @cols=@vals=();
        foreach my $key(keys %rowinfo){
            push @cols,$key;
            push @vals,$rowinfo{$key}
        }
        push @sqlcmds,"insert into ".$ods_schema.".dbo.workdateinfo".$ods_tbl_suffix." (".join(',',@cols).") values (".join(',',@vals).")";
    }
    $sql = "begin transaction;set xact_abort on;".
           "delete from ".$ods_schema.".dbo.workdateinfo".$ods_tbl_suffix." where workdate between ".$dbfmt_sdate." and ".$dbfmt_edate.";".
            join(";\n",@sqlcmds).";".
           "commit transaction;";
    my $executecmds = 1;
    $sth = $odsdbh->prepare($sql);
    if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to prepare WorkDate Info update query","Query - $sql");}
    if($executecmds == 1){
        $sth->execute;
        if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to execute WorkDate Info update query","Query - $sql");}
    }
    $sth->finish();

}#end build_workdate_info

#------------------------------------------
sub get_cpholidays{
    unless($CPHolidays){
        #Get all holidays for use.  
        #Getting all as usully this function is called repetatively, and this cuts way down in the query setup/teardown time
        my $sql = "select convert(varchar(10),nonworkingdate,101) as hdate, description, 'Y' as val from nonworkingdaysettings";
        $main::debughash{sql_Nonworking_Dates}=$sql;
        $main::debughash{aaa_nonworking_dates_runcount}+=1;
        &open_cp_dbconnection;
        my $sth = $cpdbh->prepare($sql);
        if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to prepare CP Non-Working Days query","Query - $sql");}
        $sth->execute();
        if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error trying to execute CP Non-Working Days query","Query - $sql");}
        $CPHolidays=$sth->fetchall_hashref('hdate');
        if(defined($DBI::errstr) && ($DBI::errstr ne "")){&display_query_error("Error occured during fetch of CP Non-Working Days query info","Query - $sql");}
        $sth->finish();
    }
    return($CPHolidays);
}#end get_cpholidays

#------------------------------------------
#sub open_cpqb_dbconnection{
sub open_ODS_dbconnection{
    my($userid,$passwd);
    my $connectionInfo="";
    if($ods_dbflag eq "prod"){
        $userid = "cpqb_mgr";
        $passwd = "cpqb_1234";
        $ods_schema="ost_ods";
        $ods_tbl_suffix="";
        $connectionInfo="DBI:ODBC:cpqb_mgr"; #Production DB
    }elsif($ods_dbflag eq "prod_stage"){
        $userid = "cp_builder";
        $passwd = "Bu11zi3!";
        $ods_schema="dart_staging";
        $ods_tbl_suffix="_cp";
        $connectionInfo="DBI:ODBC:prod_stage"; #Production ODS DB
    }elsif($ods_dbflag eq "test_local"){
        $userid = "cpqb_mgr";
        $passwd = "cpqb_1234";
        $ods_schema="ost_ods";
        $ods_tbl_suffix="";
        $connectionInfo="DBI:ODBC:cpqb_mgr_local"; #Local Development DB
    }elsif($ods_dbflag eq "test_server"){
        $userid = "cpqb_mgr_test";
        $passwd = "cpqbtest_9182";
        $ods_schema="ost_ods";
        $ods_tbl_suffix="";
        $connectionInfo="DBI:ODBC:cpqb_mgr_test"; #Test Env on Server
    }
    $odsdbh = DBI->connect($connectionInfo,$userid,$passwd);
    $odsdbh->{FetchHashKeyName} = 'NAME_lc';
    return($odsdbh);
}#end open_ODS_dbconnection


#------------------------------------------
sub display_query_error {
   my(@msg)=@_;
    my $display = "----------------------------\n";
    $display .= "----------------------------\n$_\n----------------------------\n" for @msg;
    $display .= "Database Error - ".($DBI::errstr)."\n";
    $display .= "----------------------------\n";
    use Carp;
    $display .= Carp::longmess."----------------------------\n";
    &show_bad_action($display);
}# end display_query_error

#------------------------------------------
sub show_bad_action {
    #This may be called from anywhere in the program where an error_message needs to be output
    #This will terminate the script.
    my($msg) = @_;
    my $display = "";
    $display .= "\n\n";
    $display .= "\n";
    $display .= "Invalid Action was requested or an Error occured!!<br>\n";
    $display .= "\n\n----------------------------\n\n";

#    if($debug){
        $display.=$msg."\n\n----------------------------\n\n";
#    else{
#        &application_error_report($display.$msg);#Send the error message to the Application Admin(s)
#        $display .= "An e-mail has been sent to the System Administrator with the necessary details<br>\n";
#        $display .= "<br><br>To Return to the main screen of the application, click ".&build_applink("here").
#                    br().br().br().hr();
#    }
    print $display;
    die;
}#end show_bad_action


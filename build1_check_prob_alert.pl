#!/usr/local/bin/perl

use strict;
use MIME::Lite;
use Time::localtime;

my($fname)=@ARGV;
open FH, $fname or die $!;
my @lines = <FH>;
close(FH);

my($msginfo,$rstat,$rmsg);
    $msginfo=();
#    $msginfo->{msgto}="rsiner\@ostusa.com;6164854679\@vtext.com";
#    $msginfo->{msgto}="rsiner\@ostusa.com";
    $msginfo->{msgto}="help\@ostusa.com";
    $msginfo->{msgtype}="TEXT";
    $msginfo->{subject}="Build1_ODS Error";
#    $msginfo->{message}="Error was detected in Build1_ODS Job. Check error log\n\n";
    $msginfo->{message}.=join("\n",@lines);
    ($rstat,$rmsg)=&send_message($msginfo);

#------------------------------------------
sub send_message {
    my($info)=@_;
    my $msgfrom = "Build1_ODS\@ostusa.com";
    my $emailsvr = "ost-emailrelay.ostusa.com";
    my $emailsendflag = "Y";
    
    if(uc($info->{msgtype}) eq "H"){$info->{message} =~ s/\n/\<br\>/g;}
    my $msg = MIME::Lite->new(
            From    =>$msgfrom,
            To      =>$info->{msgto},
            Cc      =>$info->{msgcc},
            Subject =>$info->{subject},
            Data    =>$info->{message},
            Title   =>$info->{subject},
            Type    =>'multipart/alternative'
        );
    if(uc($info->{msgtype}) eq "H"){$msg->attach(Type => "text/html",Data => "<body>".$info->{message}."</body>");}
    else{$msg->attach(Type => "TEXT",Data => $info->{message});}

    #Now, send the message, (hopefully) trapping for any errors that occur when sending the message
    my @errmsgs=();
    my $errmsg="";

    eval{$msg->send('smtp', $emailsvr, Timeout=>60)};
    if($@){
        if($errmsg ne ""){$errmsg .= "\n";}
        push @errmsgs,"Error - ".$@;
        $errmsg = $@;
    }

    $errmsg="";
    my $rstat = -1;
    if(@errmsgs){
        $errmsg = join("\n",@errmsgs);
        $rstat=-1;
    }else{
        $rstat=0;
    }

    return($rstat,$errmsg);

}#end send_message


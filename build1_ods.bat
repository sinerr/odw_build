echo off
set logdir=C:\web_files\logs

set logfile=%logdir%\build1.log
set errfile=%logfile%.error
echo log file set to - %logfile%
echo . >> %logfile%
echo -------------- Starting Run -------------- >> %logfile%
echo Starting Utilization Build1 job on %date% at %time%
echo Starting Utilization Build1 job on %date% at %time% >> %logfile%
perl build1.pl -db=prod 1>> %logfile% 2> %errfile%
perl build1.pl -db=stage 1>> %logfile% 2> %errfile%
if %ERRORLEVEL% NEQ 0 goto ERROR
echo .. >> %logfile%
Goto DONE
:ERROR
type %errfile% >> %logfile%
perl build1_check_prob_alert.pl %errfile%
:DONE
rem echo -------------- End Run -------------- >> %logfile%
echo Finished Utilization Build1 job on %date% at %time%
echo Finished Utilization Build1 job on %date% at %time% >> %logfile%
echo -------------- End Run -------------- >> %logfile%

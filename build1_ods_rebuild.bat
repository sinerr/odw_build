echo off
set logdir=C:\web_files\logs
set logfile=%logdir%\build1_rebuild.log
set errfile=%logfile%.error
echo log file set to - %logfile%
echo . >> %logfile%
echo -------------- Starting Rebuild Run -------------- >> %logfile%
echo Starting Utilization Build1 job -- Full Rebuild on %date% at %time%
echo Starting Utilization Build1 job -- Full Rebuild on %date% at %time% >> %logfile%


rem call:utilizbuild "prod" "1/1/2012" "6/30/2012"
rem call:utilizbuild "prod" "7/1/2012" "12/31/2012"
rem call:utilizbuild "prod" "1/1/2013" "6/30/2013"
rem call:utilizbuild "prod" "7/1/2013" "12/31/2013"
rem call:utilizbuild "prod" "1/1/2014" "6/30/2014"
rem call:utilizbuild "prod" "7/1/2014" "12/31/2014"
rem call:utilizbuild "prod" "1/1/2015" "6/30/2015"
rem call:utilizbuild "prod" "7/1/2015" "12/31/2015"
rem call:utilizbuild "prod" "1/1/2016" "6/30/2016"
rem call:utilizbuild "prod" "7/1/2016" "12/31/2016"
call:utilizbuild "prod" "1/1/2017" "6/30/2017"
call:utilizbuild "prod" "7/1/2017" "12/31/2017"
call:utilizbuild "prod" "1/1/2018" "6/30/2018"

rem call:utilizbuild "stage" "1/1/2012" "6/30/2012"
rem call:utilizbuild "stage" "7/1/2012" "12/31/2012"
rem call:utilizbuild "stage" "1/1/2013" "6/30/2013"
rem call:utilizbuild "stage" "7/1/2013" "12/31/2013"
rem call:utilizbuild "stage" "1/1/2014" "6/30/2014"
rem call:utilizbuild "stage" "7/1/2014" "12/31/2014"
rem call:utilizbuild "stage" "1/1/2015" "6/30/2015"
rem call:utilizbuild "stage" "7/1/2015" "12/31/2015"
rem call:utilizbuild "stage" "1/1/2016" "6/30/2016"
rem call:utilizbuild "stage" "7/1/2016" "12/31/2016"
rem call:utilizbuild "stage" "1/1/2017" "6/30/2017"
rem call:utilizbuild "stage" "7/1/2017" "12/31/2017"
rem call:utilizbuild "stage" "1/1/2018" "6/30/2018"

echo Finished Utilization Build1 job -- Full Rebuild on %date% at %time%
echo Finished Utilization Build1 job -- Full Rebuild on %date% at %time% >> %logfile%
echo -------------- Ending Rebuild Run -------------- >> %logfile%
EXIT /B

rem --------------------------------------------------------------------
rem *****************************
rem **** file purge function ****
rem *****************************
:utilizbuild
echo %date% at %time% - Starting Rebuild of DB %~1 with dates from %~2 to %~3
echo %date% at %time% - Starting Rebuild of DB %~1 with dates from %~2 to %~3 >> %logfile%
perl build1.pl -DB=%~1 -start=%~2 -end=%~3 1>> %logfile% 2> %errfile%
goto :eof